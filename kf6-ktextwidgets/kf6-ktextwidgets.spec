%global gitdate 20230829.233437
%global cmakever 5.240.0
%global commit0 007d4f91979cad22a84002dd80839dad2d0f8233
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework ktextwidgets

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 127%{?dist}
Summary: KDE Frameworks 6 Tier 3 addon with advanced text editing widgets

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  cmake(KF6Completion)
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6ConfigWidgets)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6Service)
BuildRequires:  cmake(KF6WidgetsAddons)
BuildRequires:  cmake(KF6WindowSystem)
BuildRequires:  kf6-rpm-macros
BuildRequires:  kf6-sonnet-devel

BuildRequires:  qt6-qtbase-devel
BuildRequires:  cmake(Qt6UiPlugin)

%if !0%{?bootstrap}
%if 0%{?fedora}
BuildRequires:  pkgconfig(Qt6TextToSpeech)
%endif
%endif

%description
KDE Frameworks 6 Tier 3 addon with advanced text edting widgets.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(KF6I18n)
Requires:       kf6-sonnet-devel
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6}
%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name



%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_libdir}/libKF6TextWidgets.so.*
%{_kf6_qtplugindir}/designer/*6widgets.so

%files devel

%{_kf6_includedir}/KTextWidgets/
%{_kf6_libdir}/libKF6TextWidgets.so
%{_kf6_libdir}/cmake/KF6TextWidgets/
%{_kf6_archdatadir}/mkspecs/modules/qt_KTextWidgets.pri


%changelog
