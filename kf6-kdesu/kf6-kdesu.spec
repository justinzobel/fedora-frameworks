%global gitdate 20230829.232950
%global cmakever 5.240.0
%global commit0 6ca8f2932cd011260f1d57a287c8f03630d22e53
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kdesu

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 130%{?dist}
Summary: KDE Frameworks 6 Tier 3 integration with su

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6Service)
BuildRequires:  kf6-kpty-devel
BuildRequires:  libX11-devel
BuildRequires:  qt6-qtbase-devel

%description
KDE Frameworks 6 Tier 3 integration with su for elevated privileges.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       kf6-kpty-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

%find_lang kdesu6_qt --all-name



%files -f kdesu6_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/*
%{_kf6_libdir}/libKF6Su.so.*
%{_kf6_libexecdir}/kdesu_stub
%attr(2755,root,nobody) %{_kf6_libexecdir}/kdesud

%files devel

%{_kf6_includedir}/KDESu/
%{_kf6_libdir}/libKF6Su.so
%{_kf6_libdir}/cmake/KF6Su/
%{_kf6_archdatadir}/mkspecs/modules/qt_KDESu.pri


%changelog
