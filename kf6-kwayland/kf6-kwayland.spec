%global gitdate 20230829.233501
%global cmakever 5.240.0
%global commit0 03bea7b3b805a53246987b9e4d9836b0b8ba9680
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kwayland

%global wayland_min_version 1.3

## uncomment to enable bootstrap mode
#global bootstrap 1

## currently includes no tests
%if !0%{?bootstrap}
%if 0%{?fedora}
%global tests 1
%endif
%endif

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 127%{?dist}
Summary: KDE Frameworks 6 library that wraps Client and Server Wayland libraries

License: GPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  appstream
BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  make
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtbase-static
# https://bugs.kde.org/show_bug.cgi?id=365569#c8 claims this is needed
BuildRequires:  qt6-qtbase-private-devel

BuildRequires:  qt6-qttools-devel
BuildRequires:  wayland-devel >= %{wayland_min_version}
BuildRequires:  wayland-protocols-devel

BuildRequires:  cmake(PlasmaWaylandProtocols) > 1.1
BuildRequires:  cmake(Qt6WaylandClient)

%if 0%{?tests}
BuildRequires: dbus-x11
BuildRequires: time
BuildRequires: weston
BuildRequires: xorg-x11-server-Xvfb
%endif

Requires:       kf6-filesystem

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6 \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}

%cmake_build


%install
%cmake_install


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
time \
make test ARGS="--output-on-failure --timeout 20" -C %{_target_platform} ||:
%endif



%files
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/*categories
%{_kf6_libdir}/libKF6WaylandClient.so.5*
#%%{_kf6_libdir}/libKF6WaylandServer.so.6*
%{_kf6_libdir}/libKF6WaylandClient.so.6
# not sure if this belongs here or in -devel --rex
#%%{_libexecdir}/org-kde-kf6-kwayland-testserver

%files devel
%{_kf6_includedir}/KWayland/
%{_kf6_libdir}/cmake/KF6Wayland/
%{_kf6_libdir}/libKF6WaylandClient.so
#%%{_kf6_libdir}/libKF6WaylandServer.so
%{_kf6_libdir}/pkgconfig/KF6WaylandClient.pc
%{_kf6_archdatadir}/mkspecs/modules/qt_KWaylandClient.pri
#%%{_kf6_archdatadir}/mkspecs/modules/qt_KWaylandServer.pri


%changelog
