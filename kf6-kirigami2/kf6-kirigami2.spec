%global gitdate 20230918.230836
%global cmakever 5.240.0
%global commit0 3ca3793356c1633b9bc740970a76f1139d4a6635
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kirigami

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:    kf6-%{framework}2
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary: QtQuick plugins to build user interfaces based on the KDE UX guidelines

# All LGPLv2+ except for src/desktopicons.h (GPLv2+)
License: GPLv2+
URL:     https://techbase.kde.org/Kirigami

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream paches

# filter qml provides
%global __provides_exclude_from ^%{_kf6_qmldir}/.*\\.so$

BuildRequires: extra-cmake-modules >= %{cmakever}
BuildRequires: gcc-c++
BuildRequires: kf6-rpm-macros
BuildRequires: make

BuildRequires: qt6-linguist
BuildRequires: qt6-qtbase-devel
BuildRequires: qt6-qtbase-private-devel
BuildRequires: qt6-qtdeclarative-devel
BuildRequires: qt6-qtsvg-devel

BuildRequires: cmake(Qt6Quick)
BuildRequires: cmake(Qt6ShaderTools)
BuildRequires: cmake(Qt6Core5Compat)

BuildRequires: qt6-qtbase-private-devel

BuildRequires: pkgconfig(xkbcommon)

%if 0%{?tests}
%if 0%{?fedora}
BuildRequires: appstream
%endif
BuildRequires: xorg-x11-server-Xvfb
%endif

# workaround https://bugs.kde.org/show_bug.cgi?id=395156
%if 0%{?rhel}==7
BuildRequires: devtoolset-7-toolchain
BuildRequires: devtoolset-7-gcc-c++
%endif

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
# strictly not required, but some consumers may assume/expect runtime bits to be present too
Requires:       %{name} = %{version}-%{release}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%if 0%{?rhel}==7
. /opt/rh/devtoolset-7/enable
%endif
%{cmake_kf6} \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}
%cmake_build


%install
%cmake_install

#%%find_lang_kf6 libkirigami2plugin_qt
%find_lang_kf6 libkirigami6_qt


%check
%if 0%{?tests}
## known failure(s), not sure if possible to enable opengl/glx using
## virtualized server (QT_XCB_FORCE_SOFTWARE_OPENGL doesn't seem to help)
#2/2 Test #2: qmltests .........................***Exception: Other  0.19 sec
#Could not initialize GLX
export QT_XCB_FORCE_SOFTWARE_OPENGL=1
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
make test ARGS="--output-on-failure --timeout 30" -C %{_target_platform} ||:
%endif


%files -f libkirigami6_qt.lang
# README is currently only build instructions, omit for now
#doc README.md
%dir %{_kf6_qmldir}/org/
%dir %{_kf6_qmldir}/org/kde/
%license LICENSES/*.txt
%{_kf6_libdir}/libKF6Kirigami2.so.5*
%{_kf6_libdir}/libKF6Kirigami2.so.6
%{_kf6_qmldir}/org/kde/kirigami
#%%{_kf6_qmldir}/org/kde/kirigami.2/
/usr/share/qlogging-categories6/kirigami.categories

%files devel
%dir %{_kf6_datadir}/kdevappwizard/
%dir %{_kf6_datadir}/kdevappwizard/templates/
%{_kf6_archdatadir}/mkspecs/modules/qt_Kirigami2.pri
%{_kf6_datadir}/kdevappwizard/templates/kirigami6.tar.bz2
%{_kf6_includedir}/Kirigami2/
%{_kf6_libdir}/cmake/KF6Kirigami2/
%{_kf6_libdir}/libKF6Kirigami2.so


%changelog
