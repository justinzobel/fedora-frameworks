%global gitdate 20230905.093151
%global cmakever 5.240.0
%global commit0 8ed069ddbd8a17857a320c583c015b9c2ce2c4be
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kactivities

Name:    kf6-%{framework}
Summary: A KDE Frameworks 6 Tier 3 to organize user work into separate activities
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 134%{?dist}

License: GPLv2+ and LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  boost-devel
BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6WindowSystem)
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtdeclarative-devel

Obsoletes:      kf6-kactivities-libs < 5.47.0-2
Provides:       kf6-kactivities-libs = %{version}-%{release}
Provides:       kf6-kactivities-libs = %{version}-%{release}

%description
A KDE Frameworks 6 Tier 3 API for using and interacting with Activities as a
consumer, application adding information to them or as an activity manager.

%package devel
Summary:        Developer files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
%{summary}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6

%cmake_build


%install
%cmake_install



%files
%doc README.md
%license LICENSES/*.txt
%{_kf6_bindir}/kactivities-cli6
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6Activities.so.*
%{_kf6_qmldir}/org/kde/activities/

%files devel
%{_kf6_archdatadir}/mkspecs/modules/qt_KActivities.pri
%{_kf6_includedir}/KActivities/
%{_kf6_libdir}/cmake/KF6Activities/
%{_kf6_libdir}/libKF6Activities.so
%{_kf6_libdir}/pkgconfig/KF6Activities.pc


%changelog
