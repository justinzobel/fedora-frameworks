%global gitdate 20230905.004205
%global cmakever 5.240.0
%global commit0 b59a81959e9488fe656e2e54c8a410be4641c401
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kwindowsystem

## uncomment to enable bootstrap mode
#global bootstrap 1

## currently includes no tests
%if !0%{?bootstrap} 
%if 0%{?fedora} || 0%{?epel}
%global tests 1
%endif
%endif

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 127%{?dist}
Summary: KDE Frameworks 6 Tier 1 integration module with classes for windows management

License: LGPLv2+ and MIT
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

# filter plugin provides
%global __provides_exclude_from ^(%{_kf6_plugindir}/.*\\.so)$

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  make
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtbase-private-devel
BuildRequires:  qt6-qttools-devel
#BuildRequires:  qt6-qtx11extras-devel

BuildRequires:  cmake(Qt6Qml)

BuildRequires:  pkgconfig(x11)
BuildRequires:  pkgconfig(xcb)
BuildRequires:  pkgconfig(xcb-icccm)
BuildRequires:  pkgconfig(xcb-keysyms)
BuildRequires:  pkgconfig(xkbcommon)
BuildRequires:  pkgconfig(xfixes)
BuildRequires:  pkgconfig(xrender)

%if 0%{?tests}
BuildRequires: openbox
%if 0%{?fedora}
BuildRequires: weston
%endif
BuildRequires: xorg-x11-server-Xvfb
%endif

Requires:       kf6-filesystem

%description
KDE Frameworks Tier 1 integration module that provides classes for managing and
working with windows.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6 \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}

%cmake_build


%install
%cmake_install

%find_lang_kf6 kwindowsystem6_qt


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
make test ARGS="--output-on-failure --timeout 30" -C %{_target_platform} ||:
%endif



%files -f kwindowsystem6_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6WindowSystem.so.*
%dir %{_kf6_plugindir}/kwindowsystem/
#{_kf6_plugindir}/kwindowsystem/KF6WindowSystemWaylandPlugin.so
%{_kf6_plugindir}/kwindowsystem/KF6WindowSystemX11Plugin.so
%{_kf6_qmldir}/org/kde/kwindowsystem

%files devel
%{_kf6_includedir}/KWindowSystem/
%{_kf6_libdir}/libKF6WindowSystem.so
%{_kf6_libdir}/cmake/KF6WindowSystem/
%{_kf6_archdatadir}/mkspecs/modules/qt_KWindowSystem.pri


%changelog
