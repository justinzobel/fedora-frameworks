%global gitdate 20230829.232936
%global cmakever 5.240.0
%global commit0 2280198c9a4b4f0997fc35722a5e8e2e649a68a4
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kdeclarative

# uncomment to enable bootstrap mode
%global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 130%{?dist}
Summary: KDE Frameworks 6 Tier 3 addon for Qt declarative

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

# filter qml provides
%global __provides_exclude_from ^%{_kf6_qmldir}/.*\\.so$

## upstream patches

BuildRequires: make
BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  cmake(KF6Config)
BuildRequires:  kf6-kglobalaccel-devel
BuildRequires:  cmake(KF6GuiAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6IconThemes)
BuildRequires:  cmake(KF6KIO)
BuildRequires:  cmake(KF6Notifications)
BuildRequires:  kf6-kpackage-devel
BuildRequires:  cmake(KF6WidgetsAddons)
BuildRequires:  cmake(KF6WindowSystem)
BuildRequires:  kf6-rpm-macros
BuildRequires:  cmake(Qt6ShaderTools)
BuildRequires:  libepoxy-devel

BuildRequires:  qt6-qtbase-devel

# https://bugs.kde.org/show_bug.cgi?id=365569#c8 claims this may be needed,
# so err on the side of caution
BuildRequires:  qt6-qtbase-private-devel

BuildRequires:  qt6-qtdeclarative-devel

%if 0%{?tests}
BuildRequires: dbus-x11
BuildRequires: mesa-dri-drivers
BuildRequires: time
BuildRequires: xorg-x11-server-Xvfb
%endif

%description
KDE Frameworks 6 Tier 3 addon for Qt declarative

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(KF6Config)
Requires:       kf6-kpackage-devel
Requires:       qt6-qtdeclarative-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6 \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
time \
make test ARGS="--output-on-failure --timeout 10 --verbose" -C %{_target_platform} ||:
%endif



%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
#%%{_kf6_bindir}/kpackagelauncherqml
#%%{_kf6_libdir}/libKF6Declarative.so.*
#%%{_kf6_libdir}/libKF6QuickAddons.so.*
%{_kf6_libdir}/libKF6CalendarEvents.so.*
%dir %{_kf6_qmldir}/org/
%dir %{_kf6_qmldir}/org/kde/
%{_kf6_qmldir}/org/kde/draganddrop/
%{_kf6_qmldir}/org/kde/graphicaleffects/
#%%{_kf6_qmldir}/org/kde/kconfig/
#%%{_kf6_qmldir}/org/kde/kcoreaddons/
%{_kf6_qmldir}/org/kde/kquickcontrols/
%{_kf6_qmldir}/org/kde/kquickcontrolsaddons/
%dir %{_kf6_qmldir}/org/kde/private/
%{_kf6_qmldir}/org/kde/private/kquickcontrols/
#%%{_kf6_qmldir}/org/kde/kio/
#%%{_kf6_qmldir}/org/kde/kwindowsystem/
#%%{_kf6_qmldir}/org/kde/kcm/

%files devel

%{_kf6_includedir}/KDeclarative/
#{_kf6_libdir}/libKF6Declarative.so
#{_kf6_libdir}/libKF6QuickAddons.so
%{_kf6_libdir}/libKF6CalendarEvents.so
%{_kf6_libdir}/cmake/KF6Declarative/
#{_kf6_archdatadir}/mkspecs/modules/qt_KDeclarative.pri
#{_kf6_archdatadir}/mkspecs/modules/qt_QuickAddons.pri


%changelog
