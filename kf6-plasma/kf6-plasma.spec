%global gitdate 20230917.201545
%global cmakever 5.240.0
%global commit0 fb9ce3c994946dd3957080f303bc2e1ba6556f90
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework plasma-framework

Name:    kf6-plasma
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 122%{?dist}
Summary: KDE Frameworks 6 Tier 3 framework is foundation to build a primary user interface

License: GPLv2+ and LGPLv2+ and BSD
URL:     https://invent.kde.org/frameworks/plasma

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

# hackish cache invalidation
# upstream has worked on this issue recently (.31 or .32?) so may consider dropping this -- rex
Source10: fedora-plasma-cache.sh.in

## upstream patches

# filter qml provides
%global __provides_exclude_from ^%{_kf6_qmldir}/.*\\.so$

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-kactivities-devel
BuildRequires:  kf6-kactivities-devel
BuildRequires:  kf6-karchive-devel
BuildRequires:  kf6-kdeclarative-devel
BuildRequires:  kf6-kdesu-devel
BuildRequires:  kf6-kglobalaccel-devel
BuildRequires:  kf6-kirigami2-devel
BuildRequires:  kf6-kpackage-devel
BuildRequires:  kf6-kparts-devel
BuildRequires:  kf6-rpm-macros
BuildRequires:  kf6-solid-devel
BuildRequires:  libGL-devel
BuildRequires:  libSM-devel
BuildRequires:  libX11-devel
BuildRequires:  libXScrnSaver-devel
BuildRequires:  libXext-devel
BuildRequires:  libXrender-devel
BuildRequires:  libxcb-devel
BuildRequires:  openssl-devel
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtbase-private-devel
BuildRequires:  qt6-qtdeclarative-devel
BuildRequires:  qt6-qtsvg-devel

BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6ConfigWidgets)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6DBusAddons)
BuildRequires:  cmake(KF6DocTools)
BuildRequires:  cmake(KF6GuiAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6IconThemes)
BuildRequires:  cmake(KF6KCMUtils)
BuildRequires:  cmake(KF6KIO)
BuildRequires:  cmake(KF6Notifications)
BuildRequires:  cmake(KF6Service)
BuildRequires:  cmake(KF6Svg)
BuildRequires:  cmake(KF6WidgetsAddons)
BuildRequires:  cmake(KF6WindowSystem)
BuildRequires:  cmake(KF6XmlGui)
BuildRequires:  cmake(PlasmaWaylandProtocols)
BuildRequires:  cmake(Qt6WaylandClient)

BuildRequires:  wayland-devel

BuildRequires:  pkgconfig(xkbcommon)

%if 0%{?fedora}
BuildRequires:  kf6-kwayland-devel
%endif

%if 0%{?fedora}
# https://bugzilla.redhat.com/1293415
Conflicts:      kdeplasma-addons < 5.5.0-3
%endif

# upstream name
Provides: plasma-framework = %{version}-%{release}

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
# https://bugzilla.redhat.com/1292506
Conflicts:      kapptemplates < 15.12.0-1
Requires:       %{name} = %{version}-%{release}
Requires:       kf6-kpackage-devel
Requires:       qt6-qtbase-devel
Requires:       cmake(KF6Service)
Requires:       cmake(KF6WindowSystem)
Provides:       plasma-framework-devel = %{version}-%{release}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1
install -m644 -p %{SOURCE10} .

%build
%cmake_kf6
%cmake_build

%install
%cmake_install
%find_lang %{name} --all-name --with-man --all-name

# create/own dirs
mkdir -p %{buildroot}%{_kf6_datadir}/plasma/plasmoids
mkdir -p %{buildroot}%{_kf6_qmldir}/org/kde/private

%if 0%{?fedora}
mkdir -p %{buildroot}%{_sysconfdir}/xdg/plasma-workspace/env
sed -e "s|@@VERSION@@|%{version}|g" fedora-plasma-cache.sh.in > \
  %{buildroot}%{_sysconfdir}/xdg/plasma-workspace/env/fedora-plasma-cache.sh
%endif


%files -f %{name}.lang
%dir %{_kf6_qmldir}/org/
%dir %{_kf6_qmldir}/org/kde/
%dir %{_kf6_qmldir}/org/kde/private/
%doc README.md
%lang(lt) %{_datadir}/locale/lt/LC_SCRIPTS/libplasma6/
%license LICENSES/*.txt
%{_kf6_datadir}/plasma/
%{_kf6_datadir}/qlogging-categories6/*plasma*
%{_kf6_libdir}/libKF6Plasma.so.*
%{_kf6_libdir}/libKF6PlasmaQuick.so.*
%{_kf6_plugindir}/kirigami/
%{_kf6_qmldir}/org/kde/plasma/
%{_libdir}/qt6/plugins/kf6/packagestructure
%{_libdir}/qt6/qml/org/kde/kirigami/styles/Plasma/AbstractApplicationHeader.qml
%{_libdir}/qt6/qml/org/kde/kirigami/styles/Plasma/Icon.qml

%if 0%{?fedora}
%{_sysconfdir}/xdg/plasma-workspace/env/fedora-plasma-cache.sh
%endif

#%%{_libdir}/qt6/qml/org/kde/kirigami/styles/Plasma/Theme.qml

%files devel
%dir %{_kf6_datadir}/kdevappwizard/
%{_kf6_datadir}/kdevappwizard/templates/
%{_kf6_includedir}/Plasma/
%{_kf6_includedir}/PlasmaQuick/
%{_kf6_libdir}/cmake/KF6Plasma/
%{_kf6_libdir}/cmake/KF6PlasmaQuick/
%{_kf6_libdir}/libKF6Plasma.so
%{_kf6_libdir}/libKF6PlasmaQuick.so


%changelog
