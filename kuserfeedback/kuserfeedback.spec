%global gitdate 20230828.194350
%global cmakever 5.240.0
%global commit0 5256bdeea62b4b4118e1fd2124c094c26c729a64
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})


Name:    kuserfeedback
Summary: Framework for collecting user feedback for apps via telemetry and surveys
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 122%{?dist}

License: MIT
URL:     https://invent.kde.org/libraries/%{name}
Source0: %{name}-%{shortcommit0}.tar.gz

## upstream patches

BuildRequires: cmake
BuildRequires: gcc-c++

BuildRequires: kf6-rpm-macros
BuildRequires: libappstream-glib
BuildRequires: desktop-file-utils
BuildRequires: extra-cmake-modules >= %{cmakever}

BuildRequires: cmake(Qt6Charts)
BuildRequires: cmake(Qt6Core)
BuildRequires: cmake(Qt6Network)
BuildRequires: cmake(Qt6PrintSupport)
BuildRequires: cmake(Qt6Qml)
BuildRequires: cmake(Qt6Svg)
BuildRequires: cmake(Qt6Test)
BuildRequires: cmake(Qt6Widgets)

BuildRequires: bison
BuildRequires: flex

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(Qt6Network)
Requires:       cmake(Qt6Widgets)

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        console
Summary:        Analytics and administration tool for UserFeedback servers
Requires:       %{name} = %{version}-%{release}
BuildRequires: qt6-qtbase-private-devel
Requires: qt6-qtcharts

%description    console
Analytics and administration tool for UserFeedback servers.


%prep
%autosetup -p1 -n %{name}-%{shortcommit0}


%build
%cmake_kf6 \
   -DENABLE_DOCS:BOOL=OFF \
   -DBUILD_WITH_QT6=ON -DQT_MAJOR_VERSION=6 

%cmake_build


%install
%cmake_install

%find_lang userfeedbackconsole5 --with-qt
%find_lang userfeedbackprovider5 --with-qt


%check
#appstream-util validate-relax --nonet %%{buildroot}%%{_kf6_metainfodir}/org.kde.kuserfeedback-console.appdata.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/org.kde.kuserfeedback-console.desktop


%files -f userfeedbackprovider5.lang
%doc README.md
%license COPYING.LIB
%{_bindir}/userfeedbackctl
%{_libdir}/libKUserFeedbackCoreQt6.so.1*
%{_libdir}/libKUserFeedbackWidgetsQt6.so.1*
%{_kf6_qmldir}/org/kde/userfeedback/
%{_kf6_datadir}/metainfo/org.kde.kuserfeedback-console.appdata.xml
%{_kf6_datadir}/qlogging-categories6/org_kde_UserFeedback.categories


%files devel
%{_includedir}/KUserFeedbackQt6
%{_libdir}/libKUserFeedbackCoreQt6.so
%{_libdir}/libKUserFeedbackWidgetsQt6.so
%{_kf6_libdir}/cmake/KUserFeedbackQt6
%{_kf6_archdatadir}/mkspecs/modules/qt_KUserFeedback*.pri


%files console -f userfeedbackconsole5.lang
%{_bindir}/UserFeedbackConsole
%{_datadir}/applications/org.kde.kuserfeedback-console.desktop
#%%{_kf6_metainfodir}/org.kde.kuserfeedback-console.appdata.xml


%changelog
