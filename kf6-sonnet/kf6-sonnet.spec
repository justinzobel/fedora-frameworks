%global gitdate 20230831.084417
%global cmakever 5.240.0
%global commit0 d4418504835c8d0091c30e7839ecd7039c14e882
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework sonnet

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 122%{?dist}
Summary: KDE Frameworks 6 Tier 1 solution for spell checking
License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

# filter plugin provides
%global __provides_exclude_from ^(%{_kf6_plugindir}/.*\\.so)$

%if ! 0%{?bootstrap}
BuildRequires:  hunspell-devel
%endif

BuildRequires:  appstream
BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  make
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtdeclarative-devel
BuildRequires:  qt6-qttools-devel
BuildRequires:  zlib-devel

BuildRequires:  cmake(Qt6Quick)

Requires:       kf6-filesystem
Requires:       %{name}-core = %{version}-%{release}
Requires:       %{name}-ui = %{version}-%{release}

%description
KDE Frameworks 6 Tier 1 solution for spell checking.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        core
Summary:        Non-gui part of the Sonnet framework
%description    core
Non-gui part of the Sonnet framework provides low-level spell checking tools

%package        ui
Summary:        GUI part of the Sonnet framework
Requires:       %{name}-core = %{version}-%{release}
%description    ui
GUI part of the Sonnet framework provides widgets with spell checking support.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6} \
  %{?tests:-DBUILD_TESTING:BOOL=ON}
%cmake_build

%install
%cmake_install
%find_lang_kf6 sonnet6_qt

%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
make test ARGS="--output-on-failure --timeout 300" -C %{_target_platform} ||:
%endif


%files
%doc README.md
%license LICENSES/*.txt


%files core
%{_kf6_datadir}/qlogging-categories6/*categories
%{_kf6_libdir}/libKF6SonnetCore.so.*
%if ! 0%{?bootstrap}
%dir %{_kf6_plugindir}/sonnet/
%{_kf6_plugindir}/sonnet/sonnet_hunspell.so
%endif
%{_kf6_bindir}/parsetrigrams6
%{_kf6_qmldir}/org/kde/sonnet/


%files ui -f sonnet6_qt.lang
%{_kf6_libdir}/libKF6SonnetUi.so.*
%{_kf6_qtplugindir}/designer/*6widgets.so

%files devel
%{_kf6_archdatadir}/mkspecs/modules/qt_SonnetCore.pri
%{_kf6_archdatadir}/mkspecs/modules/qt_SonnetUi.pri
%{_kf6_includedir}/Sonnet/
%{_kf6_includedir}/SonnetCore/
%{_kf6_includedir}/SonnetUi/
%{_kf6_libdir}/cmake/KF6Sonnet/
%{_kf6_libdir}/libKF6SonnetCore.so
%{_kf6_libdir}/libKF6SonnetUi.so


%changelog
