%global gitdate 20230914.195237
%global cmakever 5.240.0
%global commit0 7010522e9b71f216e45dd80af0c39a62f6f246f1
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%global framework extra-cmake-modules

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global docs 1
%global tests 1
%endif

Name:    extra-cmake-modules
Summary: Additional modules for CMake build system
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 136%{?dist}
License: BSD
URL:     https://api.kde.org/ecm/

%global versiondir %(echo %{version} | cut -d. -f1-2)
%global revision %(echo %{version} | cut -d. -f3)

Source0:        https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz
BuildArch:      noarch

## bundle clang python bindings here, at least until they are properly packaged elsewhere, see:
## https://bugzilla.redhat.com/show_bug.cgi?id=1490997
#Source1: clang-python-4.0.1.tar.gz
%if 0
%global clang 1
Provides: bundled(python2-clang) = 4.0.1
%if 0%{?tests}
BuildRequires: python2-PyQt6-devel
%endif
%endif

## upstreamable patches
# do not unconditionally link in base/core libpoppler library
Patch2: extra-cmake-modules-5.39.0-poppler_overlinking.patch
# https://bugzilla.redhat.com/1435525
Patch3: extra-cmake-modules-5.89.0-qt_prefix.patch

BuildRequires: kf6-rpm-macros
BuildRequires: make
%if 0%{?docs}
# qcollectiongenerator
BuildRequires: qt6-qttools-devel
# sphinx-build
%if 0%{?fedora} || 0%{?rhel} > 7
BuildRequires: python3-sphinx
%global sphinx_build -DSphinx_BUILD_EXECUTABLE:PATH=%{_bindir}/sphinx-build-3
%else
BuildRequires: python2-sphinx
%endif
%endif

Requires: kf6-rpm-macros
%if 0%{?fedora} || 0%{?rhel} > 7
# /usr/share/ECM/kde-modules/appstreamtest.cmake references appstreamcli
# hard vs soft dep?  --rex
Recommends: appstream
%endif
# /usr/share/ECM/modules/ECMPoQmTools.cmake
%if 0%{?fedora} || 0%{?rhel} > 7
Requires: cmake(Qt6LinguistTools)
%else
# use pkgname instead of cmake since el7 qt6 pkgs currently do not include cmake() provides
Requires: qt6-linguist
%endif

%description
Additional modules for CMake build system needed by KDE Frameworks.

%prep
%autosetup -n %{name}-%{shortcommit0} -p1

%build

%if 0%{?clang}
PYTHONPATH=`pwd`/python
export PYTHONPATH
%endif

%cmake_kf6 \
  -DBUILD_HTML_DOCS:BOOL=%{?docs:ON}%{!?docs:OFF} \
  -DBUILD_MAN_DOCS:BOOL=%{?docs:ON}%{!?docs:OFF} \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF} \
  %{?sphinx_build}

%cmake_build

%install
%cmake_install

%if 0%{?clang}
# hack clang-python install
mkdir -p %{buildroot}%{_datadir}/ECM/python/clang
install -m644 -p python/clang/* %{buildroot}%{_datadir}/ECM/python/clang/
%endif

%check
%if 0%{?tests}
%if 0%{?clang}
PYTHONPATH=`pwd`/python
export PYTHONPATH
%endif
export CTEST_OUTPUT_ON_FAILURE=1
make test ARGS="--output-on-failure --timeout 300" -C %{_target_platform} ||:
%endif

%files
%doc README.rst
%license LICENSES/*.txt
%{_datadir}/ECM/
%if 0%{?docs}
%{_kf6_docdir}/ECM/html/
%{_kf6_mandir}/man7/ecm*.7*
%endif


%changelog
