%global gitdate 20230829.232826
%global cmakever 5.240.0
%global commit0 06084642d82ea67901551a37e531ba919663cc80
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kcompletion

Name:           kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 132%{?dist}
Summary:        KDE Frameworks 6 Tier 2 addon with auto completion widgets and classes

License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream fixes

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6WidgetsAddons)
BuildRequires:  kf6-rpm-macros

BuildRequires:  pkgconfig(Qt6Widgets)
BuildRequires:  pkgconfig(xkbcommon)

BuildRequires:  cmake(Qt6UiPlugin)
BuildRequires:  cmake(KF6Codecs)

%description
KCompletion provides widgets with advanced completion support as well as a
lower-level completion class which can be used with your own widgets.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       pkgconfig(Qt6Widgets)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

%find_lang_kf6 kcompletion6_qt



%files -f kcompletion6_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6Completion.so.*
%{_kf6_qtplugindir}/designer/*6widgets.so

%files devel

%{_kf6_includedir}/KCompletion/
%{_kf6_libdir}/libKF6Completion.so
%{_kf6_libdir}/cmake/KF6Completion/
%{_kf6_archdatadir}/mkspecs/modules/qt_KCompletion.pri


%changelog
