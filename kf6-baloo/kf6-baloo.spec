%global gitdate 20230907.155247
%global cmakever 5.240.0
%global commit0 c735faf5a6a3ef3d29882552ad0a9264a294e038
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework baloo

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:    kf6-%{framework}
Summary: A Tier 3 KDE Frameworks 6 module that provides indexing and search functionality
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 134%{?dist}

# libs are LGPL, tools are GPL
# KDE e.V. may determine that future LGPL/GPL versions are accepted
License: (LGPLv2 or LGPLv3) and (GPLv2 or GPLv3)
URL:     https://community.kde.org/Baloo

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0:        https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

Source1:        97-kde-baloo-filewatch-inotify.conf
# shutdown script to explictly stop baloo_file on logout
# Now that baloo supports systemd user unit, this can probably be dropped -- rex
Source2:        baloo_file_shutdown.sh

## upstreamable patches
# http://bugzilla.redhat.com/1235026
Patch100: baloo-5.67.0-baloofile_config.patch

## upstream patches

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  kf6-kcrash-devel
BuildRequires:  cmake(KF6DBusAddons)
BuildRequires:  kf6-kfilemetadata-devel
BuildRequires:  cmake(KF6I18n)
BuildRequires:  kf6-kidletime-devel
BuildRequires:  cmake(KF6KIO)
BuildRequires:  kf6-rpm-macros
BuildRequires:  kf6-solid-devel

BuildRequires:  lmdb-devel
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtdeclarative-devel

# for systemd-related macros
BuildRequires:  systemd

%if 0%{?tests}
BuildRequires: dbus-x11
BuildRequires: time
BuildRequires: xorg-x11-server-Xvfb
%endif

Obsoletes:      kf6-baloo-tools < 5.5.95-1
Provides:       kf6-baloo-tools = %{version}-%{release}

%if 0%{?fedora}
Obsoletes:      baloo < 5
Provides:       baloo = %{version}-%{release}
%else
Conflicts:      baloo < 5
%endif

# main pkg accidentally multilib'd prior to 5.21.0-4
Obsoletes:      kf6-baloo < 5.21.0-4

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
# KDE e.V. may determine that future LGPL versions are accepted
License:        LGPLv2 or LGPLv3
Requires:       %{name}-libs = %{version}-%{release}
Requires:       cmake(KF6CoreAddons)
Requires:       kf6-kfilemetadata-devel
Requires:       qt6-qtbase-devel

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        file
Summary:        File indexing and search for Baloo
# KDE e.V. may determine that future LGPL versions are accepted
License:        LGPLv2 or LGPLv3
%if 0%{?fedora}
Obsoletes:      baloo-file < 5.0.1-2
Provides:       baloo-file = %{version}-%{release}
%else
Conflicts:      baloo-file < 5
%endif
Requires:       %{name}-libs = %{version}-%{release}
%description    file
%{summary}.

%package        libs
Summary:        Runtime libraries for %{name}
# KDE e.V. may determine that future LGPL versions are accepted
License:        LGPLv2 or LGPLv3
%description    libs
%{summary}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6} \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}
%cmake_build


%install
%cmake_install

%if 0%{?flatpak:1}
rm -fv %{buildroot}%{_userunitdir}/kde-baloo.service
%endif

# baloodb not installed unless BUILD_EXPERIMENTAL is enabled, so omit translations
rm -fv %{buildroot}%{_datadir}/locale/*/LC_MESSAGES/baloodb5.*

install -p -m644 -D %{SOURCE1} %{buildroot}%{_prefix}/lib/sysctl.d/97-kde-baloo-filewatch-inotify.conf
install -p -m755 -D %{SOURCE2} %{buildroot}%{_sysconfdir}/xdg/plasma-workspace/shutdown/baloo_file.sh

%find_lang baloodb6
%find_lang baloo_file6
%find_lang baloo_file_extractor6
%find_lang balooctl6
%find_lang balooengine6
%find_lang baloosearch6
%find_lang balooshow6
%find_lang kio6_baloosearch
%find_lang kio6_tags
%find_lang kio6_timeline

cat kio6_tags.lang kio6_baloosearch.lang kio6_timeline.lang \
    balooctl6.lang balooengine6.lang baloosearch6.lang \
    balooshow6.lang baloo_file6.lang baloo_file_extractor6.lang \
    baloodb6.lang > %{name}.lang

#cat baloo_file5.lang \
#    > %{name}-file.lang


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
time \
make test ARGS="--output-on-failure --timeout 300" -C %{_target_platform} ||:
%endif


%files -f %{name}.lang
%license LICENSES/*.txt
#{_kf6_bindir}/baloodb
%{_kf6_bindir}/baloosearch6
%{_kf6_bindir}/balooshow6
%{_kf6_bindir}/balooctl6
%{_kf6_datadir}/qlogging-categories6/%{framework}*

%files file
%{_prefix}/lib/sysctl.d/97-kde-baloo-filewatch-inotify.conf
%config(noreplace) %{_sysconfdir}/xdg/plasma-workspace/shutdown/baloo_file.sh
#%%{_kf6_bindir}/baloo_file6
#%%{_kf6_bindir}/baloo_file_extractor6
%config(noreplace) %{_kf6_sysconfdir}/xdg/autostart/baloo_file.desktop
%if ! 0%{?flatpak:1}
%{_userunitdir}/kde-baloo.service
%endif
%{_libexecdir}/kf6/baloo_file
%{_libexecdir}/kf6/baloo_file_extractor


%files libs
%license LICENSES/*
%{_kf6_libdir}/libKF6Baloo.so.*
%{_kf6_libdir}/libKF6BalooEngine.so.*
# multilib'd plugins and friends
%{_kf6_plugindir}/kio/baloosearch.so
%{_kf6_plugindir}/kio/tags.so
%{_kf6_plugindir}/kio/timeline.so
%{_kf6_plugindir}/kded/baloosearchmodule.so
%{_kf6_qmldir}/org/kde/baloo

%files devel
%{_kf6_libdir}/libKF6Baloo.so
%{_kf6_libdir}/cmake/KF6Baloo/
%{_kf6_libdir}/pkgconfig/KF6Baloo.pc
%{_kf6_includedir}/Baloo/

%{_kf6_archdatadir}/mkspecs/modules/qt_Baloo.pri
%{_kf6_datadir}/dbus-1/interfaces/org.kde.baloo.*.xml
%{_kf6_datadir}/dbus-1/interfaces/org.kde.Baloo*.xml


%changelog
