%global gitdate 20230916.153304
%global cmakever 5.240.0
%global commit0 ff6ecddb383f69ee908fb5067fc7bbd01de03220
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kio

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary: KDE Frameworks 6 Tier 3 solution for filesystem abstraction

License: GPLv2+ and MIT and BSD
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches (lookaside)

## upstreamable patches

%if 0%{?flatpak}
# Disable the help: and ghelp: protocol for Flatpak builds, to avoid depending
# on the docbook stack.
Patch101: kio-no-help-protocol.patch
%endif

# filter plugin provides
%global __provides_exclude_from ^(%{_kf6_qtplugindir}/.*\\.so)$

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
%if 0%{?rhel} && 0%{?rhel} < 9
BuildRequires: gcc-toolset-9
%endif
# core
BuildRequires:  kf6-karchive-devel
BuildRequires:  kf6-kcrash-devel
BuildRequires:  kf6-solid-devel

BuildRequires:  cmake(KF6ColorScheme)
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6DBusAddons)
BuildRequires:  cmake(KF6DocTools)
BuildRequires:  cmake(KF6GuiAddons)
BuildRequires:  cmake(KF6I18n)
#BuildRequires:  cmake(KF6KCMUtils)
BuildRequires:  cmake(KF6Service)

# extras
BuildRequires:  kf6-kbookmarks-devel
BuildRequires:  cmake(KF6Completion)
BuildRequires:  cmake(KF6ConfigWidgets)
BuildRequires:  cmake(KF6IconThemes)
BuildRequires:  cmake(KF6ItemViews)
BuildRequires:  cmake(KF6JobWidgets)
BuildRequires:  cmake(KF6WindowSystem)
# others
BuildRequires:  cmake(KF6Notifications)
BuildRequires:  kf6-ktextwidgets-devel
BuildRequires:  kf6-kwallet-devel
BuildRequires:  cmake(KF6WidgetsAddons)
BuildRequires:  cmake(KF6XmlGui)

BuildRequires:  krb5-devel
BuildRequires:  libacl-devel
%if !0%{?flatpak}
BuildRequires:  libxml2-devel
BuildRequires:  libxslt-devel
%endif
BuildRequires:  pkgconfig(blkid)
BuildRequires:  pkgconfig(mount)
BuildRequires:  pkgconfig(xkbcommon)
BuildRequires:  zlib-devel

BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtbase-private-devel
BuildRequires:  cmake(Qt6UiPlugin)
BuildRequires:  cmake(Qt6Qml)

BuildRequires:  cmake(KF6KDED)
BuildRequires:  cmake(Qt6Core5Compat)


#%%if ! 0%%{?bootstrap}
# really runtime dep, but will make cmake happier when building
#BuildRequires: kf6-kded-devel
# (apparently?) requires org.kde.klauncher5 service provided by kf6-kinit -- rex
# not versioned to allow update without bootstrap
# <skip!>
#BuildRequires:  kf6-kinit-devel
#%%endif

Requires:       %{name}-core = %{version}-%{release}
Requires:       %{name}-widgets = %{version}-%{release}
Requires:       %{name}-file-widgets = %{version}-%{release}
#Requires:       %%{name}-ntlm% = %%{version}-%%{release}
Requires:       %{name}-gui = %{version}-%{release}

Requires: kf6-kded

%if 0%{?fedora} || 0%{?rhel} > 7
%global _with_html --with-html
%endif

%description
KDE Frameworks 6 Tier 3 solution for filesystem abstraction

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       kf6-kbookmarks-devel
Requires:       cmake(KF6Completion)
Requires:       cmake(KF6Config)
Requires:       cmake(KF6CoreAddons)
Requires:       cmake(KF6ItemViews)
Requires:       cmake(KF6JobWidgets)
Requires:       cmake(KF6Service)
Requires:       kf6-solid-devel
Requires:       cmake(KF6XmlGui)
Requires:       cmake(KF6WindowSystem)
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        doc
Summary:        Documentation files for %{name}
Requires:       %{name}-core = %{version}-%{release}
Obsoletes:      kf6-kio-doc < 5.11.0-3
BuildArch:      noarch
%description    doc
Documentation for %{name}.

%package        core
Summary:        Core components of the KIO Framework
## org.kde.klauncher5 service referenced from : src/core/slave.cpp
%{?kf6_kinit_requires}
Requires:       %{name}-core-libs = %{version}-%{release}
Requires:       %{name}-doc = %{version}-%{release}
%description    core
KIOCore library provides core non-GUI components for working with KIO.

%package        core-libs
Summary:        Runtime libraries for KIO Core
Requires:       %{name}-core = %{version}-%{release}
%description    core-libs
%{summary}.

%package        widgets
Summary:        Widgets for KIO Framework
## org.kde.klauncher5 service referenced from : widgets/krun.cpp
## included here for completeness, even those -core already has a dependency.
%{?kf6_kinit_requires}
Requires:       %{name}-core = %{version}-%{release}
%description    widgets
KIOWidgets contains classes that provide generic job control, progress
reporting, etc.

%package        widgets-libs
Summary:        Runtime libraries for KIO Widgets library
Requires:       %{name}-widgets = %{version}-%{release}
%description    widgets-libs
%{summary}.

%package        file-widgets
Summary:        Widgets for file-handling for KIO Framework
Requires:       %{name}-widgets = %{version}-%{release}
%description    file-widgets
The KIOFileWidgets library provides the file selection dialog and
its components.

%package        gui
Summary:        Gui components for the KIO Framework
Requires:       %{name}-core = %{version}-%{release}
%description    gui
%{summary}.

#%%package        ntlm
#Summary:        NTLM support for KIO Framework
#%%description    ntlm
#KIONTLM provides support for NTLM authentication mechanism in KIO


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%if 0%{?rhel} && 0%{?rhel} < 9    
. /opt/rh/gcc-toolset-9/enable    
%endif 
%cmake_kf6

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name --with-man %{?_with_html}
# Temporary, likely to moving to Konqueror https://invent.kde.org/network/konqueror/-/issues/4
rm -f %{buildroot}%{_bindir}/kcookiejar5
rm -f %{buildroot}%{_datadir}/dbus-1/services/org.kde.kcookiejar5.service

%files
%license LICENSES/*.txt
%doc README.md

%files core
%{_kf6_sysconfdir}/xdg/accept-languages.codes
%{_kf6_datadir}/qlogging-categories6/*categories
%{_kf6_libexecdir}/kio_http_cache_cleaner
%{_kf6_libexecdir}/kpac_dhcp_helper
%{_kf6_libexecdir}/kioexec
#%%{_kf6_libexecdir}/kioslave6
%{_kf6_libexecdir}/kiod6
%{_kf6_libexecdir}/kioworker
%{_kf6_bindir}/ktelnetservice6
%{_kf6_bindir}/ktrash6
%{_kf6_plugindir}/kio/
%{_kf6_plugindir}/kded/
#%%{_kf6_qtplugindir}/kcm_*.so
%{_kf6_plugindir}/kiod/
%{_kf6_datadir}/kf6/searchproviders/*.desktop
%{_kf6_datadir}/knotifications6/proxyscout.*
#%%{_kf6_datadir}/kf6/kcookiejar/domain_info
%{_kf6_datadir}/applications/*.desktop
%{_kf6_datadir}/kconf_update/*
%{_datadir}/dbus-1/services/org.kde.*.service

## omitted since 5.45, security concerns? -- rex
%if 0
# file_helper
%{_kf6_sysconfdir}/dbus-1/system.d/org.kde.kio.file.conf
%{_kf6_libexecdir}/kauth/file_helper
%{_kf6_datadir}/dbus-1/system-services/org.kde.kio.file.service
%{_kf6_datadir}/polkit-1/actions/org.kde.kio.file.policy
%endif


%files core-libs
%{_kf6_libdir}/libKF6KIOCore.so.*

%files doc -f %{name}.lang
#%%{_kf6_mandir}/man8/kcookiejar5.8*
%if !0%{?_with_html:1}
%{_kf6_docdir}/HTML/*/*
%endif


%files gui
%{_kf6_libdir}/libKF6KIOGui.so.*

%files widgets
#{_kf6_datadir}/kservices6/fixhosturifilter.desktop
#{_kf6_datadir}/kservices6/kshorturifilter.desktop
#{_kf6_datadir}/kservices6/kuriikwsfilter.desktop
#{_kf6_datadir}/kservices6/kurisearchfilter.desktop
#{_kf6_datadir}/kservices6/localdomainurifilter.desktop
%config %{_kf6_sysconfdir}/xdg/kshorturifilterrc
%dir %{_kf6_plugindir}/urifilters/
#%%{_kf6_datadir}/kservices6/searchproviders
#%%{_kf6_datadir}/kservices6/webshortcuts.desktop
#%%{_kf6_datadir}/kservicetypes6/*.desktop
%{_kf6_plugindir}/urifilters/*.so
#%%{_kf6_qtplugindir}/kcm_webshortcuts.so
#%%{_kf6_qtplugindir}/plasma/kcms/systemsettings/kcm_smb.so
#%%{_kf6_qtplugindir}/plasma/kcms/systemsettings_qwidgets/kcm_*.so


%files widgets-libs
%{_kf6_libdir}/libKF6KIOWidgets.so.*
%{_kf6_qtplugindir}/designer/*6widgets.so


%files file-widgets
%{_kf6_libdir}/libKF6KIOFileWidgets.so.*

#%%ldconfig_scriptlets ntlm

#%%files ntlm
#%%{_kf6_libdir}/libKF6KIONTLM.so.*

%files devel
#%%{_datadir}/dbus-1/interfaces/*.xml
%{_kf6_archdatadir}/mkspecs/modules/qt_*.pri
#%%{_kf6_bindir}/protocoltojson
%{_kf6_datadir}/kdevappwizard/templates/kioworker6.tar.bz2
%{_kf6_includedir}/*
%{_kf6_libdir}/*.so
%{_kf6_libdir}/cmake/KF6KIO/

%changelog
