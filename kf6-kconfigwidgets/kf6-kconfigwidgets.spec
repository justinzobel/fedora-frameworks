%global gitdate 20230911.213947
%global cmakever 5.240.0
%global commit0 87f19cafc75990b24ff58c33296f0d577101a61e
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kconfigwidgets

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 131%{?dist}
Summary: KDE Frameworks 6 Tier 3 addon for creating configuration dialogs

License: GPLv2+ and LGPLv2+ and MIT
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
%global revision %(echo %{version} | cut -d. -f3)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-kauth-devel
BuildRequires:  kf6-kcodecs-devel
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6DocTools)
BuildRequires:  cmake(KF6GuiAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6WidgetsAddons)
BuildRequires:  kf6-rpm-macros

BuildRequires:  cmake(KF6ColorScheme)

# KColorScheme requires color schemes to be installed
# https://pagure.io/fedora-workstation/issue/314
Requires:       plasma-breeze-common

BuildRequires:  qt6-qtbase-devel
BuildRequires:  cmake(Qt6UiPlugin)

%description
KConfigWidgets provides easy-to-use classes to create configuration dialogs, as
well as a set of widgets which uses KConfig to store their settings.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       kf6-kauth-devel
Requires:       kf6-kcodecs-devel
Requires:       cmake(KF6Config)
Requires:       cmake(KF6WidgetsAddons)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

%find_lang %{name} --with-man --all-name



%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}*
%{_kf6_libdir}/qt6/plugins/designer/kconfigwidgets6widgets.so
%{_kf6_libdir}/libKF6ConfigWidgets.so.*
## fixme: %%lang'ify these -- rex
#%%{_kf6_datadir}/locale/*/kf6_entry.desktop
#%%{_kf6_qtplugindir}/designer/*6widgets.so
#%%{_kf6_plugindir}/designer/kconfigwidgets6widgets.so
#%%{_kf6_datadir}/locale/en_US/kf6_entry.desktop
%{_datadir}/locale/*/kf6_entry.desktop

%files devel
#%%{_kf6_bindir}/preparetips6
#%%{_kf6_mandir}/man1/preparetips6.1*

%{_kf6_includedir}/KConfigWidgets/
%{_kf6_libdir}/libKF6ConfigWidgets.so
%{_kf6_libdir}/cmake/KF6ConfigWidgets/
%{_kf6_archdatadir}/mkspecs/modules/qt_KConfigWidgets.pri


%changelog
