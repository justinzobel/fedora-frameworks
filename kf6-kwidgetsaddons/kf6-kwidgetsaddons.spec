%global gitdate 20230917.131236
%global cmakever 5.240.0
%global commit0 de81f37b07205f3c2992b1d484895367dc7f7d6a
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kwidgetsaddons

Name:           kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 127%{?dist}
Summary:        KDE Frameworks 6 Tier 1 addon with various classes on top of QtWidgets

License:        GPLv2+ and LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:        https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qttools-devel
BuildRequires:  qt6-qttools-static

Requires:       kf6-filesystem

%description
KDE Frameworks 6 Tier 1 addon with various classes on top of QtWidgets.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

%find_lang_kf6 kwidgetsaddons6_qt



%files -f kwidgetsaddons6_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/*categories
%{_kf6_libdir}/libKF6WidgetsAddons.so.*
%{_kf6_datadir}/kf6/kcharselect/
%{_kf6_qtplugindir}/designer/*6widgets.so

%files devel

%{_kf6_includedir}/KWidgetsAddons/
%{_kf6_libdir}/libKF6WidgetsAddons.so
%{_kf6_libdir}/cmake/KF6WidgetsAddons/
%{_kf6_archdatadir}/mkspecs/modules/qt_KWidgetsAddons.pri


%changelog
