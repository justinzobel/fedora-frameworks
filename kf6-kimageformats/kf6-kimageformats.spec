%global gitdate 20230917.080747
%global cmakever 5.240.0
%global commit0 7899c27a804c655c37027721132d8eb07416fd17
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kimageformats

Name:           kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary:        KDE Frameworks 6 Tier 1 addon with additional image plugins for QtGui

License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

%global __provides_exclude_from ^(%{_kf6_plugindir}/.*\\.so)$

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  jasper-devel
BuildRequires:  kf6-rpm-macros

%if 0%{?fedora}
BuildRequires:  pkgconfig(libavif)
%endif

# Update to work with OpenEXR/Imath 3.
# Fails to build for f33 and EPEL due to lack of kf6-rpm-macros and
# extra-cmake-modules.
%if 0%{?fedora} > 34
BuildRequires:  cmake(OpenEXR)
BuildRequires:  cmake(Imath)
%else
BuildRequires:  pkgconfig(OpenEXR)
%endif
BuildRequires:  cmake(KF6Archive)
BuildRequires:  pkgconfig(zlib)
BuildRequires:  qt6-qtbase-devel

Requires:       kf6-filesystem

%description
This framework provides additional image format plugins for QtGui.  As
such it is not required for the compilation of any other software, but
may be a runtime requirement for Qt-based software to support certain
image formats.

%prep
%autosetup -n %{framework}-%{commit0} -p1

%build
%{cmake_kf6}
%cmake_build

%install
%cmake_install

%files
%doc README.md
%license LICENSES/*.txt
%{_kf6_qtplugindir}/imageformats/*.so
#%%{_kf6_datadir}/kservices6/qimageioplugins/*.desktop

%changelog
