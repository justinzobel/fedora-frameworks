%global gitdate 20230918.185141
%global cmakever 5.240.0
%global commit0 87f26fd14daed7733493e13f47253067bd14649d
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework ktexteditor

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 127%{?dist}
Summary: KDE Frameworks 6 Tier 3 with advanced embeddable text editor

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches (lookaside cache)

## upstreamable patches

# filter plugin provides
%global __provides_exclude_from ^(%{_kf6_qtplugindir}/.*\\.so)$

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-karchive-devel
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6GuiAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6IconThemes)
BuildRequires:  cmake(KF6KIO)
BuildRequires:  kf6-kparts-devel
BuildRequires:  kf6-rpm-macros
BuildRequires:  kf6-sonnet-devel
BuildRequires:  kf6-syntax-highlighting-devel

BuildRequires:  pkgconfig(Qt6Widgets)
BuildRequires:  pkgconfig(Qt6PrintSupport)
BuildRequires:  pkgconfig(Qt6Qml)
BuildRequires:  pkgconfig(Qt6Xml)
#BuildRequires:  pkgconfig(Qt6XmlPatterns)

BuildRequires:  pkgconfig(libgit2) >= 0.22.0

%if 0%{?fedora}
BuildRequires:  pkgconfig(editorconfig)
%endif

%if 0%{?tests}
#BuildRequires: pkgconfig(Qt6Script)
BuildRequires: dbus-x11
BuildRequires: time
BuildRequires: xorg-x11-server-Xvfb
%endif

%description
KTextEditor provides a powerful text editor component that you can embed in your
application, either as a KPart or using the KF6::TextEditor library (if you need
more control).

The text editor component contains many useful features, from syntax
highlighting and automatic indentation to advanced scripting support, making it
suitable for everything from a simple embedded text-file editor to an advanced
IDE.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       kf6-kparts-devel
Requires:       kf6-syntax-highlighting-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6 \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name

# create/own dirs
mkdir -p %{buildroot}%{_kf6_qtplugindir}/ktexteditor


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
time \
make test ARGS="--output-on-failure --timeout 300" -C %{_target_platform} ||:
%endif



%files -f %{name}.lang
#%%{_kf6_datadir}/katepart6/
#%%{_kf6_datadir}/kservices6/katepart.desktop
#%%{_kf6_datadir}/kservicetypes6/*.desktop
%dir %{_kf6_plugindir}/parts/
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/dbus-1/system-services/org.kde.ktexteditor.katetextbuffer.service
%{_kf6_datadir}/dbus-1/system.d/org.kde.ktexteditor.katetextbuffer.conf
%{_kf6_datadir}/katepart5/script/README.md
%{_kf6_datadir}/polkit-1/actions/org.kde.ktexteditor.katetextbuffer.policy
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6TextEditor.so.*
%{_kf6_libexecdir}/kf6/kauth/kauth_ktexteditor_helper
%{_kf6_plugindir}/parts/katepart.so
%{_kf6_qtplugindir}/ktexteditor/

%files devel
%{_kf6_archdatadir}/mkspecs/modules/qt_KTextEditor.pri
%{_kf6_datadir}/kdevappwizard/templates/ktexteditor6-plugin.tar.bz2
%{_kf6_includedir}/KTextEditor/
%{_kf6_libdir}/cmake/KF6TextEditor/
%{_kf6_libdir}/libKF6TextEditor.so

%changelog
