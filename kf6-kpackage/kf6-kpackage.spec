%global gitdate 20230829.233254
%global cmakever 5.240.0
%global commit0 92e3a0a215210a611a82e926f3866af596caab31
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kpackage

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:           kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 128%{?dist}
Summary:        KDE Frameworks 6 Tier 2 library to load and install packages as plugins

License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1,2)

Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  kf6-karchive-devel
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6I18n)
# optional
BuildRequires:  cmake(KF6DocTools)

BuildRequires:  qt6-qtbase-devel
BuildRequires: make

%description
KDE Frameworks 6 Tier 2 library to load and install non-binary packages as
if they were plugins.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       extra-cmake-modules
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6} \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}
%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name --with-man

# create/own dirs
mkdir -p %{buildroot}%{_kf6_qtplugindir}/kpackage/packagestructure/
mkdir -p %{buildroot}%{_kf6_datadir}/kpackage/


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
make test ARGS="--output-on-failure --timeout 30" -C %{_target_platform} ||:
%endif



%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6Package.so.*
%{_kf6_qtplugindir}/kpackage/
%{_kf6_datadir}/kpackage/
#%%{_kf6_datadir}/kservicetypes6/kpackage-*.desktop
%{_kf6_bindir}/kpackagetool6
%{_mandir}/man1/kpackagetool6.1*

%files devel

%{_kf6_includedir}/KPackage/
%{_kf6_libdir}/libKF6Package.so
%{_kf6_libdir}/cmake/KF6Package/


%changelog
