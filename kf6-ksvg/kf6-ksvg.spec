%global gitdate 20230905.161844
%global cmakever 5.240.0
%global commit0 f49e0f8d7e62035816cb83166c966a7fa58377e5
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%global framework ksvg
%global base_name ksgv

Name:    kf6-ksvg
Summary: Components for handling SVGs
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 105%{?dist}

License: GPL-2.0-or-later and LGPL-2.0-or-later
URL:     https://invent.kde.org/libraries/%{framework}
Source0: %{framework}-%{shortcommit0}.tar.gz

BuildRequires: cmake
BuildRequires: gcc-c++

BuildRequires: kf6-rpm-macros
BuildRequires: extra-cmake-modules >= %{cmakever}

BuildRequires: cmake(Qt6)
BuildRequires: cmake(Qt6Quick)
BuildRequires: cmake(Qt6Svg)

BuildRequires: cmake(KF6Archive)
BuildRequires: cmake(KF6Config)
BuildRequires: cmake(KF6CoreAddons)
BuildRequires: cmake(KF6GuiAddons)
BuildRequires: cmake(KF6Kirigami2)
BuildRequires: cmake(KF6ColorScheme)

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -p1 -n %{framework}-%{shortcommit0}

%build
%cmake_kf6 \
   -DBUILD_WITH_QT6=ON -DQT_MAJOR_VERSION=6 

%cmake_build


%install
%cmake_install


%files
%license LICENSES/*
%{_kf6_libdir}/libKF6Svg.so.*
%{_kf6_libdir}/qt6/qml/org/kde/ksvg
%{_kf6_datadir}/qlogging-categories6/ksvg.categories


%files devel
%{_kf6_includedir}/KSvg
%{_kf6_libdir}/cmake/KF6Svg
%{_kf6_libdir}/libKF6Svg.so


%changelog
