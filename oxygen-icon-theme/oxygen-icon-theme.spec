%global gitdate 20230829.233713
%global cmakever 5.240.0
%global commit0 41a0aa92ad4d01eb466600496d434b4b9f45df0a
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework oxygen-icons5

# trim changelog included in binary rpms
%global _changelog_trimtime %(date +%s -d "1 year ago")

%if ! 0%{?bootstrap}
## enable icon optimizations, takes awhile
%global optimize 0
%endif

## allow building with an older extra-cmake-modules
%global kf6_version 5.27.0

Name:    oxygen-icon-theme
Summary: Oxygen icon theme
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 122%{?dist}
Epoch:   1

# http://techbase.kde.org/Policies/Licensing_Policy
License: LGPLv3+
URL:     https://techbase.kde.org/Projects/Oxygen

%global versiondir %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz
BuildArch: noarch

# we are noarch, skip trying to find debuginfo
%global debug_package   %{nil}

## upstreamable patches

BuildRequires:  kf6-rpm-macros
BuildRequires:  extra-cmake-modules >= %{?kf6_version}%{!?kf6_version:%{version}}
BuildRequires:  qt6-qtbase-devel

BuildRequires:  hardlink
%if 0%{?optimize}
# for optimizegraphics
BuildRequires:  kde-dev-scripts
%endif
BuildRequires:  kde-filesystem
BuildRequires:  time

# inheritance, though could consider Recommends: if needed -- rex
Requires: hicolor-icon-theme

# upstream names
Provides:       oxygen-icons5 = %{version}-%{release}
Provides:       oxygen-icons = %{version}-%{release}
Provides:       kf6-oxygen-icons = %{version}-%{release}

# some icons moved here from kdepim, add explicit Conflicts to help dep solvers
%if 0%{?fedora} < 24
# http://bugzilla.redhat.com/1308475
Conflicts: kmail < 4.14.10-10
%else
# http://bugzilla.redhat.com/1308358
Conflicts: kmail < 15.12.2
%endif

%description
%{summary}.


%prep
%autosetup -n %{framework}-%{commit0} -p1

%if 0%{?kf6_version:1}
sed -i -e "s|%{version}|%{kf6_version}|g" CMakeLists.txt
%endif


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

# optimize
pushd %{buildroot}%{_kf6_datadir}/icons/oxygen

du -s  .

hardlink -c -v %{buildroot}%{_kf6_datadir}/icons/oxygen

du -s .

%if 0%{?optimize}
time optimizegraphics

du -s .

## As of 15.04.3, hardlink reports
#Directories 78
#Objects 6926
#IFREG 6848
#Comparisons 901
#Linked 901
#saved 7737344
hardlink -c -v %{buildroot}%{_kf6_datadir}/icons/oxygen

du -s .
%endif
popd

# create/own all potential dirs
mkdir -p %{buildroot}%{_kf6_datadir}/icons/oxygen/{16x16,22x22,24x24,32x32,36x36,48x48,64x64,96x96,128x128,512x512,scalable}/{actions,apps,devices,mimetypes,places}


%if 0%{?fedora} > 25
## trigger-based scriptlets
%transfiletriggerin -- %{_datadir}/icons/oxygen
gtk-update-icon-cache --force %{_datadir}/icons/oxygen &>/dev/null || :

%transfiletriggerpostun -- %{_datadir}/icons/oxygen
gtk-update-icon-cache --force %{_datadir}/icons/oxygen &>/dev/null || :

%else
# classic scriptlets
%post
touch --no-create %{_kf6_datadir}/icons/oxygen &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_kf6_datadir}/icons/oxygen &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
touch --no-create %{_kf6_datadir}/icons/oxygen &> /dev/null || :
gtk-update-icon-cache %{_kf6_datadir}/icons/oxygen &> /dev/null || :
fi
%endif

%files
%doc AUTHORS CONTRIBUTING
%license COPYING
%dir %{_datadir}/icons/oxygen/
%{_datadir}/icons/oxygen/index.theme
%{_datadir}/icons/oxygen/base/
%{_datadir}/icons/oxygen/*x*/
%{_datadir}/icons/oxygen/scalable/


%changelog
