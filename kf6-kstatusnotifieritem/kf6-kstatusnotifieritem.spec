%global gitdate 20230829.233400
%global cmakever 5.240.0
%global commit0 5d65d17cca892d42f3ff2193ad16066dbdca32bb
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})


%global framework kstatusnotifieritem

Name:           kf6-%{framework}
Version:        %{cmakever}^%{gitdate}.%{shortcommit0}
Release:        48%{?dist}
Summary:        Implementation of Status Notifier Items

License:        LGPL-2.0-or-later
URL:            https://invent.kde.org/frameworks/kstatusnotifieritem
Source0:        %{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  cmake(Qt6Widgets)
BuildRequires:  cmake(Qt6DBus)
BuildRequires:  cmake(KF6WindowSystem)
BuildRequires:  pkgconfig(x11)

%description
%summary.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{framework}-%{commit0}

%build
%cmake_kf6 -DBUILD_WITH_QT6=ON
%cmake_build

%install
%cmake_install

%files
/usr/lib64/libKF6StatusNotifierItem.so.*
/usr/lib64/qt6/mkspecs/modules/qt_KStatusNotifierItem.pri
/usr/share/dbus-1/interfaces/kf6_org.kde.StatusNotifierItem.xml
/usr/share/dbus-1/interfaces/kf6_org.kde.StatusNotifierWatcher.xml
/usr/share/locale/ca/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/ca@valencia/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/eo/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/es/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/eu/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/fr/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/gl/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/ia/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/ka/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/ko/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/nl/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/nn/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/sl/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/tr/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/uk/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/locale/zh_CN/LC_MESSAGES/kstatusnotifieritem6_qt.qm
/usr/share/qlogging-categories6/kstatusnotifieritem.categories

%files devel
/usr/include/KF6/KStatusNotifierItem
/usr/lib64/cmake/KF6StatusNotifierItem
/usr/lib64/libKF6StatusNotifierItem.so

%changelog
