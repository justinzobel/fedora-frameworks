%global gitdate 20230829.233218
%global cmakever 5.240.0
%global commit0 4df934c9540336568bd211306a80d97fd80deced
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kjobwidgets

Name:           kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary:        KDE Frameworks 6 Tier 2 addon for KJobs

License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
%global revision %(echo %{version} | cut -d. -f3)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  libX11-devel
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtbase-private-devel
BuildRequires:  qt6-qttools-devel

BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6Notifications)
BuildRequires:  cmake(KF6WidgetsAddons)

BuildRequires:  pkgconfig(xkbcommon)

%description
KDE Frameworks 6 Tier 2 addon for KJobs

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
Requires:       cmake(KF6CoreAddons)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6}
%cmake_build


%install
%cmake_install

%find_lang_kf6 kjobwidgets6_qt



%files -f kjobwidgets6_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6JobWidgets.so.*

%files devel

%{_kf6_includedir}/KJobWidgets/
%{_kf6_libdir}/libKF6JobWidgets.so
%{_kf6_libdir}/cmake/KF6JobWidgets/
%{_kf6_datadir}/dbus-1/interfaces/*.xml
%{_kf6_archdatadir}/mkspecs/modules/qt_KJobWidgets.pri


%changelog
