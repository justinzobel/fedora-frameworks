%global gitdate 20230829.233536
%global cmakever 5.240.0
%global commit0 d91d4e52ca50bf31736f3d262e28a7d50c746a05
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%undefine __cmake_in_source_build
%global framework kxmlgui

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 127%{?dist}
Summary: KDE Frameworks 6 Tier 3 solution for user-configurable main windows

# Library LGPLv2+, ksendbugmail is GPLv2+
License: GPLv2+ and LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:        https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-attica-devel
BuildRequires:  kf6-kglobalaccel-devel
BuildRequires:  kf6-ktextwidgets-devel
BuildRequires:  kf6-rpm-macros
BuildRequires:  libX11-devel
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtbase-private-devel

BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6ConfigWidgets)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6GuiAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6IconThemes)
BuildRequires:  cmake(KF6ItemViews)
BuildRequires:  cmake(KF6WidgetsAddons)
BuildRequires:  cmake(KF6WindowSystem)
BuildRequires:  cmake(Qt6UiPlugin)

%description
KDE Frameworks 6 Tier 3 solution for user-configurable main windows.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(KF6Config)
Requires:       cmake(KF6ConfigWidgets)
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1

%build
%cmake_kf6
%cmake_build

%install
%cmake_install
# Own the kxmlgui directory
mkdir -p %{buildroot}%{_kf6_datadir}/kxmlgui5/
%find_lang %{name} --all-name

%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
#%%dir %{_kf6_sysconfdir}/xdg/ui/
#%%config %{_kf6_sysconfdir}/xdg/ui/ui_standards.rc
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6XmlGui.so.*
#%%{_kf6_libexecdir}/ksendbugmail
%dir %{_kf6_datadir}/kxmlgui5/
%{_kf6_qtplugindir}/designer/*6widgets.so

%files devel
%{_kf6_includedir}/KXmlGui/
%{_kf6_libdir}/libKF6XmlGui.so
%{_kf6_libdir}/cmake/KF6XmlGui/
%{_kf6_archdatadir}/mkspecs/modules/qt_KXmlGui.pri


%changelog
