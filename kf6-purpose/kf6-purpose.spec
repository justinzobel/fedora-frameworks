%global gitdate 20230906.122109
%global cmakever 5.240.0
%global commit0 9cabf08da160a79c5625a57bf302db31e961536e
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework purpose

Name:    kf6-purpose
Summary: Framework for providing abstractions to get the developer's purposes fulfilled
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 122%{?dist}

# KDE e.V. may determine that future GPL versions are accepted
# most files LGPLv2+, configuration.cpp is KDE e.V. GPL variant
License: GPLv2 or GPLv3
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## downstream patches
# src/quick/CMakeLists.txt calls 'cmake' directly, use 'cmake3' instead (mostly for epel7)
%if 0%{?rhel} && 0%{?rhel} < 8
Patch100: purpose-5.79.0-cmake3.patch
%endif

# filter plugin provides
%global __provides_exclude_from ^(%{_kf6_qtplugindir}/.*\\.so)$

BuildRequires: extra-cmake-modules >= %{cmakever}
BuildRequires: gcc-c++
BuildRequires: gettext
BuildRequires: intltool

BuildRequires: kf6-rpm-macros
BuildRequires: cmake(KF6Config)
BuildRequires: cmake(KF6CoreAddons)
BuildRequires: cmake(KF6I18n)
BuildRequires: cmake(KF6KIO)
BuildRequires: kf6-kirigami2-devel

BuildRequires: cmake(KF6Kirigami2)

# optional sharefile plugin
BuildRequires: cmake(KF6KIO)
BuildRequires: cmake(KF6Notifications)

BuildRequires: pkgconfig(Qt6Network)
BuildRequires: pkgconfig(Qt6Qml)

#%%if 0%%{?fedora} || 0%%{?rhel} > 7
#BuildRequires: kaccounts-integration-devel
# runtime dep?
#BuildRequires: kde-connect
BuildRequires: pkgconfig(accounts-qt6)
#BuildRequires: pkgconfig(libaccounts-glib)
#%%endif

%description
Purpose offers the possibility to create integrate services and actions on
any application without having to implement them specifically. Purpose will
offer them mechanisms to list the different alternatives to execute given the
requested action type and will facilitate components so that all the plugins
can receive all the information they need.

%package  devel
Summary:  Development files for %{name}
Requires: %{name} = %{version}-%{release}
Requires: cmake(KF6CoreAddons)
%description devel
%{summary}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name

## unpackaged files
# omit (unused?) conflicting icons with older kamoso (rename to "google-youtube"?)
rm -fv %{buildroot}%{_datadir}/icons/hicolor/*/actions/kipiplugin_youtube.png



%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6Purpose.so.5*
%{_kf6_libdir}/libKF6Purpose.so.6
%{_kf6_libdir}/libKF6PurposeWidgets.so.5*
%{_kf6_libdir}/libKF6PurposeWidgets.so.6
%{_kf6_libdir}/libPhabricatorHelpers.so.5*
%{_kf6_libdir}/libPhabricatorHelpers.so.6
%{_kf6_libdir}/libReviewboardHelpers.so.5*
%{_kf6_libdir}/libReviewboardHelpers.so.6
%{_kf6_libexecdir}/purposeprocess
%{_kf6_datadir}/kf6/purpose/
%{_kf6_plugindir}/purpose/
%dir %{_kf6_plugindir}/kfileitemaction/
%{_kf6_plugindir}/kfileitemaction/sharefileitemaction.so
%{_kf6_qmldir}/org/kde/purpose/
# this conditional may require adjusting too (e.g. wrt %%twitter)
#%%if 0%%{?fedora} || 0%%{?rhel} > 7
#%%{_kf6_datadir}/accounts/services/kde/google-youtube.service
#%%{_kf6_datadir}/accounts/services/kde/nextcloud-upload.service
#%%endif
%{_datadir}/icons/hicolor/*/apps/*-purpose.*
#{_datadir}/icons/hicolor/*/actions/google-youtube.*

%files devel
%{_kf6_libdir}/libKF6Purpose.so
%{_kf6_libdir}/libKF6PurposeWidgets.so
%{_kf6_includedir}/Purpose/
%{_kf6_includedir}/PurposeWidgets/
#%%{_kf6_libdir}/cmake/KDEExperimentalPurpose/
%{_kf6_libdir}/cmake/KF6Purpose/


%changelog
