%global gitdate 20231003.073019
%global cmakever 5.240.0
%global commit0 a25ccfaeb0be427ab132b251b9c5b2bd6dce6fb3
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kcodecs

Name:           kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 144%{?dist}
Summary:        KDE Frameworks 6 Tier 1 addon with string manipulation methods

License:        GPLv2+ and LGPLv2+ and BSD
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  gperf
BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qttools-devel

Requires:       kf6-filesystem

%description
KDE Frameworks 6 Tier 1 addon with string manipulation methods.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6}

%cmake_build


%install
%cmake_install

%find_lang_kf6 kcodecs6_qt



%files -f kcodecs6_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/*categories
%{_kf6_libdir}/libKF6Codecs.so.*

%files devel

%{_kf6_includedir}/KCodecs/
%{_kf6_libdir}/libKF6Codecs.so
%{_kf6_libdir}/cmake/KF6Codecs/
#%%{_kf6_archdatadir}/mkspecs/modules/qt_KCodecs.pri


%changelog
* Sat Oct 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20231001.134036.0e2dad1-144
- rebuilt

* Sun Oct 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-143
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-142
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-141
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-140
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-139
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-138
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-137
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-136
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-135
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-134
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232811.ea56b58-133
- rebuilt

