%global gitdate 20230829.233237
%global cmakever 5.240.0
%global commit0 779e0dc3b0302fd708e27cf35f9c5fcc75f39657
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework knotifications

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary: KDE Frameworks 6 Tier 2 solution with abstraction for system notifications

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz


## upstream patches

#BuildRequires:  dbusmenu-qt6-devel
BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-kcodecs-devel
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6WindowSystem)
BuildRequires:  kf6-rpm-macros

BuildRequires:  phonon-qt6-devel
BuildRequires:  pkgconfig(libcanberra)

BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qttools-devel
#BuildRequires:  qt6-qtx11extras-devel
BuildRequires:  cmake(Qt6Quick)

%if !0%{?bootstrap}
%if 0%{?fedora}
BuildRequires:  pkgconfig(Qt6TextToSpeech)
%endif
%endif

%if 0%{?tests}
BuildRequires: dbus-x11
BuildRequires: time
BuildRequires: xorg-x11-server-Xvfb
%endif

%description
KDE Frameworks 6 Tier 3 solution with abstraction for system
notifications.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1

%build
%cmake_kf6 \
  %{?tests:-DBUILD_TESTING:BOOL=ON}
%cmake_build

%install
%cmake_install
%find_lang_kf6 knotifications6_qt
# We own the folder
mkdir -p %{buildroot}/%{_kf6_datadir}/knotifications6

%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a dbus-launch --exit-with-session \
time make test -C %{_target_platform} ARGS="--output-on-failure --timeout 300" ||:
%endif

%files -f knotifications6_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6Notifications.so.*
%{_kf6_datadir}/knotifications6/
/usr/lib64/qt6/qml/org/kde/notification/libknotificationqmlplugin.so
/usr/lib64/qt6/qml/org/kde/notification/qmldir

%files devel
%{_kf6_includedir}/KNotifications/
%{_kf6_libdir}/libKF6Notifications.so
%{_kf6_libdir}/cmake/KF6Notifications/
#%%{_kf6_datadir}/dbus-1/interfaces/*.xml
%{_kf6_archdatadir}/mkspecs/modules/qt_KNotifications.pri

%changelog
