%global gitdate 20230915.204758
%global cmakever 5.240.0
%global commit0 8e10b56b1a859c9edfbee7d38ceab0fe6e3bebdc
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kdoctools

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary: KDE Frameworks 6 Tier 2 addon for generating documentation

License: GPLv2+ and MIT
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  docbook-dtds
BuildRequires:  docbook-style-xsl

BuildRequires:  kf6-rpm-macros
BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-karchive-devel
BuildRequires:  cmake(KF6I18n)

BuildRequires:  libxml2-devel
BuildRequires:  libxslt-devel
%if 0%{?fedora} || 0%{?rhel} > 7
%global _with_html --with-html
BuildRequires:  perl-generators
%endif
%if 0%{?fedora} || 0%{?epel} > 7
%global perl_uri_escape perl(Any::URI::Escape)
%else
%global perl_uri_escape perl(URI::Escape)
%endif
BuildRequires:  %{perl_uri_escape}
BuildRequires:  qt6-qtbase-devel

Requires:       docbook-dtds
Requires:       docbook-style-xsl

%description
Provides tools to generate documentation in various format from DocBook files.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Provides:       kf6-kdoctools-static = %{version}-%{release}
Requires:       qt6-qtbase-devel
Requires:       %{perl_uri_escape}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6 \
    %{?tests:-DBUILD_TESTING:BOOL=ON}

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name --with-man %{?_with_html}


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
make test -C %{_target_platform} ARGS="--output-on-failure --timeout 300" ||:
%endif



%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_libdir}/libKF6DocTools.so.5*
%{_kf6_libdir}/libKF6DocTools.so.6
## FIXME/TODO: which of these to move to -devel -- rex
%{_kf6_bindir}/checkXML6
%{_kf6_bindir}/meinproc6
%{_kf6_mandir}/man1/*.1*
%{_kf6_mandir}/man7/*.7*
%{_kf6_datadir}/kf6/kdoctools/
%if !0%{?_with_html:1}
%{_kf6_docdir}/HTML/*/kdoctools5-common/
%endif

%files devel
%{_kf6_includedir}/KDocTools/
%{_kf6_libdir}/libKF6DocTools.so
%{_kf6_libdir}/cmake/KF6DocTools/


%changelog
