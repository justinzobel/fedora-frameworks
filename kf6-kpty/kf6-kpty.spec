%global gitdate 20230829.233329
%global cmakever 5.240.0
%global commit0 6af12f144961a2bd6feb762bdb78b6194810c0c4
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kpty

Name:           kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 128%{?dist}
Summary:        KDE Frameworks 6 Tier 2 module providing Pty abstraction

License:        LGPLv2+ and GPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  libutempter-devel
BuildRequires:  qt6-qtbase-devel

# runtime calls %%_libexexdir/utempter/utempter
Requires:       libutempter

%description
KDE Frameworks 6 tier 2 module providing Pty abstraction.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(KF6CoreAddons)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
# find_program for utempter is failing for some reason, so
# set path explicitly to known-good value
%cmake_kf6 \
  -DUTEMPTER_EXECUTABLE:PATH=/usr/libexec/utempter/utempter

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name --with-man



%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6Pty.so.5*
%{_kf6_libdir}/libKF6Pty.so.6

%files devel

%{_kf6_includedir}/KPty/
%{_kf6_libdir}/libKF6Pty.so
%{_kf6_libdir}/cmake/KF6Pty/
%{_kf6_archdatadir}/mkspecs/modules/qt_KPty.pri


%changelog
