%global gitdate 20230914.123515
%global cmakever 5.240.0
%global commit0 5203bed8bfb5c46ea3f2539d909fafc24a692ce3
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kservice

Name:    kf6-%{framework}
Summary: KDE Frameworks 6 Tier 3 solution for advanced plugin and service introspection
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 127%{?dist}

# mixture of LGPLv2 and LGPLv2+ (mostly the latter)
License: LGPLv2
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches

## downstream patches
# Fedora customizations to the menu categories
# adds the Administration menu from redhat-menus which equals System + Settings
# This prevents the stuff getting listed twice, under both System and Settings.
Patch100:  kservice-5.15.0-xdg-menu.patch

# kbuildsycoca5 always gives:
# kf6.kservice.sycoca: Parse error in  "$HOME/.config/menus/applications-merged/xdg-desktop-menu-dummy.menu" , line  1 , col  1 :  "unexpected end of file"
# hide that by default, make it qCDebug instead (of qCWarning)
Patch101:  kservice-5.17.0-vfolder_spam.patch


BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  kf6-kcrash-devel
BuildRequires:  cmake(KF6DBusAddons)
BuildRequires:  cmake(KF6DocTools)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel

BuildRequires:  flex
BuildRequires:  bison

# for the Administration category
Recommends:       redhat-menus

%description
KDE Frameworks 6 Tier 3 solution for advanced plugin and service
introspection.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(KF6Config)
Requires:       cmake(KF6CoreAddons)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6}
%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name --with-man

mv %{buildroot}%{_kf6_sysconfdir}/xdg/menus/applications.menu \
   %{buildroot}%{_kf6_sysconfdir}/xdg/menus/kf6-applications.menu

mkdir -p %{buildroot}%{_kf6_datadir}/kservices6
mkdir -p %{buildroot}%{_kf6_datadir}/kservicetypes6



%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
# this is not a config file, despite rpmlint complaining otherwise -- rex
%{_kf6_sysconfdir}/xdg/menus/kf6-applications.menu
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_bindir}/kbuildsycoca6
%{_kf6_libdir}/libKF6Service.so.6*
%{_kf6_libdir}/libKF6Service.so.5.240.0
%{_kf6_datadir}/kservicetypes6/
%{_kf6_datadir}/kservices6/
%{_kf6_mandir}/man8/*.8*

%files devel

%{_kf6_includedir}/KService/
%{_kf6_libdir}/libKF6Service.so
%{_kf6_libdir}/cmake/KF6Service/
%{_kf6_archdatadir}/mkspecs/modules/qt_KService.pri


%changelog
