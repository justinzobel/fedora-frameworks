%global gitdate 20230929.080333
%global cmakever 5.240.0
%global commit0 00b95500d6f528f67d270880ae36241e95d8e46a
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kconfig

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
#global docs 1
#global python_bindings 1
%global tests 1

%endif

%if 0%{?flatpak}
%global docs 0
%endif

# use ninja instead of make
%global ninja 1

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 156%{?dist}
Summary: KDE Frameworks 6 Tier 1 addon with advanced configuration system
License: GPL-2.0-or-later AND LGPL-2.0-or-later AND (LGPL-2.0-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL) AND BSD-2-Clause AND BSD-3-Clause AND CC0-1.0 AND MIT
URL:     https://invent.kde.org/frameworks/%{framework}
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{commit0}.tar.gz

# Compile Tools
BuildRequires:  cmake
BuildRequires:  gcc-c++
%if 0%{?ninja}
BuildRequires:  ninja-build
%endif

# KDE Frameworks
BuildRequires:  extra-cmake-modules >= %{cmakever}

# Fedora
BuildRequires:  kf6-rpm-macros

# Qt
BuildRequires:  cmake(Qt6DBus)
BuildRequires:  cmake(Qt6Gui)
BuildRequires:  cmake(Qt6Qml)
BuildRequires:  cmake(Qt6Xml)

# Python Bindings
%if 0%{?python_bindings}
BuildRequires:  clang
BuildRequires:  clang-devel
BuildRequires:  python3-clang
BuildRequires:  python3-PyQt6-devel
%else
Obsoletes: python3-pykf6-%{framework} < %{version}-%{release}
Obsoletes: pykf6-%{framework}-devel < %{version}-%{release}
%endif
Obsoletes: python2-pykf6-%{framework} < %{version}

%if 0%{?tests}
BuildRequires: dbus-x11
BuildRequires: time
BuildRequires: xorg-x11-server-Xvfb
%endif

Requires:       kf6-filesystem
Requires:       %{name}-core = %{version}-%{release}
Requires:       %{name}-gui = %{version}-%{release}

%description
KDE Frameworks 6 Tier 1 addon with advanced configuration system made of two
parts: KConfigCore and KConfigGui.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       pkgconfig(Qt6Xml)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        core
Summary:        Non-GUI part of KConfig framework
Requires:       kde-settings
%description    core
KConfigCore provides access to the configuration files themselves. It features
centralized definition and lock-down (kiosk) support.

%package        gui
Summary:        GUI part of KConfig framework
Requires:       %{name}-core = %{version}-%{release}
%description    gui
KConfigGui provides a way to hook widgets to the configuration so that they are
automatically initialized from the configuration and automatically propagate
their changes to their respective configuration files.

%if 0%{?docs}
%package doc
Summary: API documentation for %{name}
BuildRequires: doxygen
BuildRequires: qt6-qdoc
BuildRequires: qt6-qhelpgenerator
BuildRequires: qt6-qtbase-doc
BuildRequires: make
Requires: kf6-filesystem
BuildArch: noarch
%description doc
%{summary}.
%endif

%if 0%{?python_bindings}
%package -n python3-pykf6-%{framework}
Summary: Python3 bindings for %{framework}
Requires: %{name} = %{version}-%{release}
%description -n python3-pykf6-%{framework}
%{summary}.

%package -n pykf6-%{framework}-devel
Summary: SIP files for %{framework} Python bindings
BuildArch: noarch
%description -n pykf6-%{framework}-devel
%{summary}.
%endif

%prep
%autosetup -n %{framework}-%{commit0} -p1

%build
%if 0%{?python_bindings:1}
PYTHONPATH=%{_datadir}/ECM/python
export PYTHONPATH
%endif

%cmake_kf6 \
  %if 0%{?flatpak}
  %{?docs:-DBUILD_QCH:BOOL=OFF} \
  %else
  %{?docs:-DBUILD_QCH:BOOL=ON} \
  %endif
  %{?ninja:-G Ninja} \
  %{?tests:-DBUILD_TESTING:BOOL=ON}

%cmake_build || \
cat %{__cmake_builddir}/autotests/kconfig_compiler/test5.cpp

%install
%cmake_install
%find_lang_kf6 kconfig6_qt

%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
## cant use %%ninja_test here for some reason, doesn't inherit env vars from xvfb or dbus -- rex
xvfb-run -a \
%if 0%{?ninja}
ninja test %{?_smp_mflags} -v -C redhat-linux-build ||:
%else
make test %{?_smp_mflags} -C redhat-linux-build ARGS="--output-on-failure --timeout 300" ||:
%endif
%endif

%files
%doc DESIGN README.md TODO
%license LICENSES/*.txt

%files core -f kconfig6_qt.lang
%{_kf6_bindir}/kreadconfig6
%{_kf6_bindir}/kwriteconfig6
%{_kf6_datadir}/qlogging-categories6/%{framework}*
%{_kf6_libdir}/libKF6ConfigCore.so.5.240.0
%{_kf6_libdir}/libKF6ConfigCore.so.6
%{_kf6_libdir}/libKF6ConfigGui.so.5.240.0
%{_kf6_libdir}/libKF6ConfigGui.so.6
%{_kf6_libdir}/libKF6ConfigQml.so.5.240.0
%{_kf6_libdir}/libKF6ConfigQml.so.6
%{_kf6_libdir}/qt6/qml/org/kde/config/kconfigqmlplugin.qmltypes
%{_kf6_libdir}/qt6/qml/org/kde/config/kde-qmlmodule.version
%{_kf6_libdir}/qt6/qml/org/kde/config/libkconfigqmlplugin.so
%{_kf6_libdir}/qt6/qml/org/kde/config/qmldir
%{_kf6_libexecdir}/kconf_update
%{_kf6_libexecdir}/kconfig_compiler_kf6

%files devel
%{_kf6_includedir}/KConfig/
%{_kf6_includedir}/KConfigCore/
%{_kf6_includedir}/KConfigGui/
%{_kf6_includedir}/KConfigQml/
%{_kf6_libdir}/cmake/KF6Config/
%{_kf6_libdir}/libKF6ConfigCore.so
%{_kf6_libdir}/libKF6ConfigGui.so
%{_kf6_libdir}/libKF6ConfigQml.so

%if 0%{?docs}
%files doc
%{_qt6_docdir}/KF6Config.qch
%{_qt6_docdir}/KF6Config.tags
%endif

%if 0%{?python_bindings}
%files -n python3-pykf6-%{framework}
%{python3_sitearch}/PyKF6/

%files -n pykf6-%{framework}-devel
%{_datadir}/sip/PyKF6/
%endif

%changelog
* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230929.080333.00b9550-156
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230925.220329.8ff3313-155
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230925.220329.8ff3313-154
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230925.220329.8ff3313-153
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230925.220329.8ff3313-152
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230925.220329.8ff33136df67fddc9dd5bd979acf81592bfe4f98-151
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230925.220329.8ff33136df67fddc9dd5bd979acf81592bfe4f98-150
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230925.220329.8ff33136df67fddc9dd5bd979acf81592bfe4f98-149
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230925.220329.8ff33136df67fddc9dd5bd979acf81592bfe4f98-148
- rebuilt

