%global gitdate 20230807.231311
%global cmakever 5.240.0
%global commit0 404b27a56564bd4d8e648b1e40ad15c01f587085
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kmoretools

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary: KDE library to support for downloading application assets from the network

License: BSD-2-Clause and CC0-1.0 and LGPL-2.1-only and LGPL-2.1-or-later and LGPL-3.0-only and LicenseRef-KDE-Accepted-LGPL
URL:     https://invent.kde.org/libraries/%{framework}
Source0: https://invent.kde.org/libraries/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++

BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6JobWidgets)
BuildRequires:  cmake(KF6KIO)
BuildRequires:  cmake(KF6Service)
BuildRequires:  cmake(KF6WidgetsAddons)

BuildRequires:  cmake(Qt6Gui)
BuildRequires:  cmake(Qt6Qml)
BuildRequires:  cmake(Qt6Quick)
BuildRequires:  cmake(Qt6Widgets)
BuildRequires:  cmake(Qt6Xml)

%description
%summary.

%package        devel
Summary:        Development files for %{name}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{framework}-%{commit0} -p1

%build
%cmake_kf6

%cmake_build

%install
%cmake_install

%find_lang %{name} --all-name

%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/kf6/kmoretools/presets-kmoretools
%{_kf6_libdir}/libKF6MoreTools.so

%files devel
%{_kf6_includedir}/KMoreTools
%{_kf6_includedir}/kmoretools_version.h
%{_kf6_libdir}/cmake/KF6MoreTools
%{_kf6_libdir}/libKF6MoreTools.so.5.240.0
%{_kf6_libdir}/libKF6MoreTools.so.6

%changelog
