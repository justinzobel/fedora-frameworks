%global gitdate 20231001.140713
%global cmakever 5.240.0
%global commit0 bd1e3454018aef01e965e0027f316789736be83e
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kitemmodels

Name:           kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 141%{?dist}
Summary:        KDE Frameworks 6 Tier 1 addon with item models

License:        LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros

BuildRequires:  pkgconfig(Qt6Core)
BuildRequires:  pkgconfig(Qt6Qml)

Requires:       kf6-filesystem

%description
KDE Frameworks 6 Tier 1 addon with item models.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6}
%cmake_build


%install
%cmake_install



%files
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6ItemModels.so.*
%{_kf6_qmldir}/org/kde/kitemmodels/

%files devel

%{_kf6_includedir}/KItemModels/
%{_kf6_libdir}/libKF6ItemModels.so
%{_kf6_libdir}/cmake/KF6ItemModels/
#%%{_kf6_archdatadir}/mkspecs/modules/qt_KItemModels.pri


%changelog
* Sat Oct 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20231001.140713.bd1e345-141
- rebuilt

* Sun Oct 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.133622.4c5c663-140
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.133622.4c5c663-139
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.133622.4c5c663-138
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.133622.4c5c663-137
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.133622.4c5c663-136
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.133622.4c5c663-135
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.133622.4c5c663-134
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.133622.4c5c663-133
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.133622.4c5c663-132
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230914.133622.4c5c663-131
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230912.184221.cfb0da9-130
- rebuilt

