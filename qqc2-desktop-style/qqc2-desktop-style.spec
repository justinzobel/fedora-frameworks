%global gitdate 20230918.193030
%global cmakever 5.240.0
%global commit0 23351429df473cfbff69ed830c8d52a8282f852f
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%global framework qqc2-desktop-style

Name:    %{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 122%{?dist}
Summary: QtQuickControls2 style for consistency between QWidget and QML apps 
# kirigami-plasmadesktop-integration: LGPLv2+
# plugin,org.kde.desktop: LGPLv3 or GPLv3
License: (LGPLv3 or GPLv3) and LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires: extra-cmake-modules >= %{cmakever}
BuildRequires: gcc-c++
BuildRequires: kf6-kirigami2-devel
BuildRequires: kf6-rpm-macros

BuildRequires: cmake(KF6ConfigWidgets)
BuildRequires: cmake(KF6Kirigami2)

BuildRequires: pkgconfig(Qt6Gui)
BuildRequires: pkgconfig(Qt6Quick)
BuildRequires: pkgconfig(Qt6Widgets)

Requires:      kf6-kirigami2

# WORKAROUND FTBFS
%if 0%{?rhel}==7
BuildRequires: devtoolset-7-toolchain
BuildRequires: devtoolset-7-gcc-c++
%endif

%description
This is a style for QtQuickControls 2 that uses QWidget's QStyle for
painting, making possible to achieve an higher degree of consistency
between QWidget-based and QML-based apps.


%prep
%autosetup -n %{framework}-%{commit0} -p1

%build
%if 0%{?rhel}==7
. /opt/rh/devtoolset-7/enable
%endif

%cmake_kf6
%cmake_build

%install
%cmake_install

%files 
%doc README.md
%license LICENSES/*.txt
%{_kf6_libdir}/cmake/KF6QQC2DesktopStyle/
%{_qt6_qmldir}/org/kde/desktop/
%{_qt6_qmldir}/org/kde/qqc2desktopstyle/


%changelog
