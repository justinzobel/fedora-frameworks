%global gitdate 20231003.075926
%global cmakever 5.240.0
%global commit0 9e228b5c812e460f5a182d238df66df709df7ed7
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kcalendarcore

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
%global tests 1
%endif

Name:    kf6-kcalendarcore
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 145%{?dist}
Summary: KDE Frameworks 6 Tier 1 KCalendarCore Library

License: LGPLv2+ and GPLv3+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

# libical (and thus kcalendarcore) not on all arches for RHEL8.
%if 0%{?rhel} == 8
ExclusiveArch: x86_64 ppc64le aarch64 %{arm}
%endif

BuildRequires: make
BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros

BuildRequires:  bison
BuildRequires:  libical-devel

BuildRequires:  qt6-qtbase-devel

%if 0%{?tests}
BuildRequires: dbus-x11
BuildRequires: xorg-x11-server-Xvfb
%endif

%description
%{summary}.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = :%{version}-%{release}
Requires:       libical-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6 \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}

%cmake_build


%install
%cmake_install

## TODO: poke upstream about failures seen on f30
#The following tests FAILED:
#         39 - testicaltimezones (Failed)
#        472 - Compat-libical3-AppleICal_1.5.ics (Failed)
#        473 - Compat-libical3-Evolution_2.8.2_timezone_test.ics (Failed)
#        475 - Compat-libical3-KOrganizer_3.1a.ics (Failed)
#        477 - Compat-libical3-MSExchange.ics (Failed)
#        478 - Compat-libical3-Mozilla_1.0.ics (Failed)
%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
make test ARGS="--output-on-failure --timeout 20" -C %{_target_platform} ||:
%endif



%files
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/*kcalendarcore.*
%{_kf6_libdir}/libKF6CalendarCore.so.*

%files devel
#%%{_kf6_includedir}/kcal*core_version.h
%{_kf6_includedir}/KCalendarCore/
%{_kf6_libdir}/libKF6CalendarCore.so
%{_kf6_libdir}/cmake/KF6CalendarCore/
%{_kf6_libdir}/pkgconfig/KF6CalendarCore.pc
#%%{_kf6_archdatadir}/mkspecs/modules/qt_KCalendarCore.pri


%changelog
* Sat Oct 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20231001.133212.aba6350-145
- rebuilt

* Sun Oct 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-144
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-143
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-142
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-141
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-140
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-139
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-138
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-137
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-136
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-135
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.232751.2905599-134
- rebuilt

