%global gitdate 20230829.232912
%global cmakever 5.240.0
%global commit0 5401330cac65066673535e231ab9379c8cb3ac14
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kcrash

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 130%{?dist}
Summary: KDE Frameworks 6 Tier 2 addon for handling application crashes

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6WindowSystem)
BuildRequires:  kf6-rpm-macros

BuildRequires:  libX11-devel
BuildRequires:  qt6-qtbase-devel
#BuildRequires:  qt6-qtx11extras-devel

%description
KCrash provides support for intercepting and handling application crashes.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6} \
  %{?fedora:-DKCRASH_CORE_PATTERN_RAISE:BOOL=OFF}
%cmake_build


%install
%cmake_install



%files
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6Crash.so.*

%files devel

%{_kf6_includedir}/KCrash/
%{_kf6_libdir}/libKF6Crash.so
%{_kf6_libdir}/cmake/KF6Crash/
%{_kf6_archdatadir}/mkspecs/modules/qt_KCrash.pri


%changelog
