%global gitdate 20230829.233208
%global cmakever 5.240.0
%global commit0 77b6030cfd52e1bfa539672f5a1f16fe45e1c3c8
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kitemviews

Name:           kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary:        KDE Frameworks 6 Tier 1 addon with item views

License:        GPLv2+ and LGPLv2+
URL:            https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qttools-devel

Requires:       kf6-filesystem

%description
KDE Frameworks 6 Tier 1 addon with item views.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

%find_lang_kf6 kitemviews6_qt



%files -f kitemviews6_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/*categories
%{_kf6_libdir}/libKF6ItemViews.so.*
%{_kf6_qtplugindir}/designer/*6widgets.so

%files devel

%{_kf6_includedir}/KItemViews/
%{_kf6_libdir}/libKF6ItemViews.so
%{_kf6_libdir}/cmake/KF6ItemViews/
%{_kf6_archdatadir}/mkspecs/modules/qt_KItemViews.pri


%changelog
