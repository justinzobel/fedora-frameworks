%global gitdate 20231003.073450
%global cmakever 5.240.0
%global commit0 ee63432bbb8bbd2914948913b98fbfded124d76d
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework prison

Name:    kf6-%{framework}
Summary: KDE Frameworks 6 Tier 1 barcode library
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 134%{?dist}

License: BSD
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros

BuildRequires:  pkgconfig(Qt6Gui)
BuildRequires:  pkgconfig(Qt6Quick)
BuildRequires:  pkgconfig(Qt6Multimedia)

BuildRequires:  cmake(ZXing)
BuildRequires:  pkgconfig(libdmtx)
BuildRequires:  pkgconfig(libqrencode)

Requires: kf6-filesystem

%description
Prison is a Qt-based barcode abstraction layer/library that provides
an uniform access to generation of barcodes with data.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6}
%cmake_build


%install
%cmake_install



%files
%doc README*
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6Prison.so.5*
%{_kf6_libdir}/libKF6Prison.so.6
%{_kf6_qmldir}/org/kde/prison/

%files devel
%{_kf6_includedir}/Prison/
%{_kf6_libdir}/libKF6Prison.so
%{_kf6_libdir}/cmake/KF6Prison/
%{_kf6_archdatadir}/mkspecs/modules/qt_Prison.pri


%changelog
* Sat Oct 07 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20231001.123735.fb12b84-134
- rebuilt

* Sun Oct 01 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230926.000236.d99e5a2-133
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230926.000236.d99e5a2-132
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230926.000236.d99e5a2-131
- rebuilt

* Sat Sep 30 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230926.000236.d99e5a2-130
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230926.000236.d99e5a2-129
- rebuilt

* Fri Sep 29 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230926.000236.d99e5a2-128
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230926.000236.d99e5a2-127
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230926.000236.d99e5a2-126
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230926.000236.d99e5a2-125
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230926.000236.d99e5a2-124
- rebuilt

* Thu Sep 28 2023 Justin Zobel <justin.zobel@gmail.com> - 5.240.0^20230829.233734.f6c6764-123
- rebuilt

