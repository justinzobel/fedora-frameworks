%global gitdate 20230829.232558
%global cmakever 5.240.0
%global commit0 4e09a15b47bc901af5c0839715aa6d7d3c331343
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework attica
# doc generation fails on 5.92, disable
# Error in line 1: Opening and ending tag mismatch.
#global docs 1

%if 0%{?flatpak}
%global docs 0
%endif

Name:   kf6-attica
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 134%{?dist}
Summary: KDE Frameworks Tier 1 Addon with Open Collaboration Services API

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel

Requires: kf6-filesystem

%description
Attica is a Qt library that implements the Open Collaboration Services
API version 1.4.

%package devel
Summary: Development files for %{name}
Requires: %{name} = %{version}-%{release}
Requires: qt6-qtbase-devel
%description devel
%{summary}.

%if 0%{?docs}
%package doc
Summary: API documentation for %{name}
BuildRequires: doxygen
BuildRequires: qt6-qdoc
BuildRequires: qt6-qhelpgenerator
BuildRequires: qt6-qtbase-doc
Requires: kf6-filesystem
BuildArch: noarch
%description doc
%{summary}.
%endif


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6 \
  %if 0%{?flatpak}
  %{?docs:-DBUILD_QCH:BOOL=OFF} \
  %else
  %{?docs:-DBUILD_QCH:BOOL=ON} \
  %endif

%cmake_build


%install
%cmake_install



%files
%doc AUTHORS ChangeLog README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6Attica.so.*

%files devel
%{_kf6_libdir}/cmake/KF6Attica/
%{_kf6_includedir}/Attica/
%{_kf6_libdir}/libKF6Attica.so
%{_kf6_archdatadir}/mkspecs/modules/qt_Attica.pri
#%%{_kf6_libdir}/pkgconfig/libKF6Attica.pc
%{_kf6_libdir}/pkgconfig/KF6Attica.pc

%if 0%{?docs}
%{_qt6_docdir}/KF6Attica.qch
%{_qt6_docdir}/KF6Attica.tags
%endif


%changelog
