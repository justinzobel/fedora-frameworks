%global gitdate 20230916.051657
%global cmakever 5.240.0
%global commit0 692826b3e2d06c1fdeea4f9d01d7fac4dedf4684
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kcmutils

# uncomment to enable bootstrap mode
#global bootstrap 1

%if !0%{?bootstrap}
## currently includes no tests, consider re-enabling when it does
#global tests 1
%endif

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 132%{?dist}
Summary: KDE Frameworks 6 Tier 3 addon with extra API to write KConfigModules

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6IconThemes)
BuildRequires:  cmake(KF6ItemViews)
BuildRequires:  kf6-kpackage-devel
BuildRequires:  cmake(KF6XmlGui)
BuildRequires:  kf6-rpm-macros
BuildRequires:  cmake(KF6KIO)

BuildRequires:  cmake(KF6Activities)
BuildRequires:  cmake(KF6GuiAddons)
BuildRequires:  cmake(KF6WindowSystem)

BuildRequires:  pkgconfig(xkbcommon)

BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtbase-private-devel
BuildRequires:  qt6-qtdeclarative-devel

%if 0%{?tests}
BuildRequires: dbus-x11
BuildRequires: time
BuildRequires: xorg-x11-server-Xvfb
%endif

%description
KCMUtils provides various classes to work with KCModules. KCModules can be
created with the KConfigWidgets framework.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(KF6ConfigWidgets)
Requires:       cmake(KF6Service)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6 \
  -DBUILD_TESTING:BOOL=%{?tests:ON}%{!?tests:OFF}

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name

# create/own dirs
mkdir -p %{buildroot}%{_kf6_qtplugindir}/kcms


%check
%if 0%{?tests}
export CTEST_OUTPUT_ON_FAILURE=1
xvfb-run -a \
dbus-launch --exit-with-session \
time \
make test ARGS="--output-on-failure --timeout 300" -C %{_target_platform} ||:
%endif



%files -f %{name}.lang
#%%{_kf6_datadir}/kservicetypes6/*.desktop
%doc README.md
%license LICENSES/*.txt
%{_kf6_bindir}/kcmshell6
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6KCMUtils.so.*
%{_kf6_libdir}/libKF6KCMUtilsCore.so.*
%{_kf6_qmldir}/org/kde/kcmutils/
%{_kf6_qtplugindir}/kcms/
/usr/lib64/libKF6KCMUtilsQuick.so.5.240.0
/usr/lib64/libKF6KCMUtilsQuick.so.6

%files devel
%{_kf6_includedir}/KCMUtils/
%{_kf6_includedir}/KCMUtilsCore/
%{_kf6_libdir}/libKF6KCMUtils.so
/usr/lib64/libKF6KCMUtilsQuick.so
%{_kf6_libdir}/libKF6KCMUtilsCore.so
%{_kf6_libdir}/cmake/KF6KCMUtils/
%{_kf6_archdatadir}/mkspecs/modules/qt_KCMUtils.pri
%{_kf6_libexecdir}/kcmdesktopfilegenerator
/usr/include/KF6/KCMUtilsQuick/KQuickConfigModule
/usr/include/KF6/KCMUtilsQuick/KQuickConfigModuleLoader
/usr/include/KF6/KCMUtilsQuick/KQuickManagedConfigModule
/usr/include/KF6/KCMUtilsQuick/kcmutilsquick_export.h
/usr/include/KF6/KCMUtilsQuick/kquickconfigmodule.h
/usr/include/KF6/KCMUtilsQuick/kquickconfigmoduleloader.h
/usr/include/KF6/KCMUtilsQuick/kquickmanagedconfigmodule.h


%changelog
