%global gitdate 20230829.232707
%global cmakever 5.240.0
%global commit0 4ae5637f44bfc934f189062c26cefb412dce7096
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kactivities-stats

Name:    kf6-%{framework}
Summary: A KDE Frameworks 6 Tier 3 library for accessing the usage data collected by the activities system
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 134%{?dist}

# KDE e.V. may determine that future GPL versions are accepted
License: LGPLv2 or LGPLv3
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches

BuildRequires:  boost-devel
BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-kactivities-devel
BuildRequires:  cmake(KF6Config)
BuildRequires:  kf6-rpm-macros
BuildRequires:  pkgconfig

BuildRequires:  qt6-qtdeclarative-devel
BuildRequires:  qt6-qtbase-devel

%description
%{summary}.

%package devel
Summary:  Developer files for %{name}
Requires: %{name} = %{version}-%{release}
Requires: qt6-qtbase-devel
%description devel
%{summary}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6 \
  -DBUILD_TESTING=ON

%cmake_build


%install
%cmake_install


# Currently includes no tests
%check
%ctest ||:



%files
%doc MAINTAINER README.developers TODO
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6ActivitiesStats.so.*

%files devel
%{_kf6_includedir}/KActivitiesStats/
%{_kf6_libdir}/cmake/KF6ActivitiesStats/
%{_kf6_libdir}/libKF6ActivitiesStats.so
%{_kf6_libdir}/pkgconfig/KF6ActivitiesStats.pc
%{_qt6_archdatadir}/mkspecs/modules/qt_KActivitiesStats.pri


%changelog
