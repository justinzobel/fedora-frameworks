%global gitdate 20230916.183112
%global cmakever 5.240.0
%global commit0 599f3e5328dfdd2083658c32f607b13003ab4a84
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework krunner

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 127%{?dist}
Summary: KDE Frameworks 6 Tier 3 solution with parallelized query system

License: LGPLv2+ and BSD
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches

# filter qml provides
%global __provides_exclude_from ^%{_kf6_qmldir}/.*\\.so$

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros

BuildRequires:  cmake(KF6Config)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6KIO)
BuildRequires:  cmake(KF6Service)
#BuildRequires:  kf6-plasma-devel
BuildRequires:  kf6-solid-devel
BuildRequires:  kf6-threadweaver-devel

BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtdeclarative-devel

BuildRequires:  cmake(KF6ItemModels)

%description
KRunner provides a parallelized query system extendable via plugins.

%package        devel
Summary:        Development files for %{name}
# krunner template moved here
Conflicts:      kapptemplate < 16.03.80
Requires:       %{name} = %{version}-%{release}
Requires:       kf6-plasma-devel
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6 -DBUILD_WITH_QT6=ON -DQT_MAJOR_VERSION=6 

%cmake_build


%install
%cmake_install



%files
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}*
%{_kf6_libdir}/libKF6Runner.so.*
#%%{_kf6_qmldir}/org/kde/krunner/libkrunnerquickplugin.so
#%%{_kf6_qmldir}/org/kde/krunner/qmldir
#%%{_kf6_qmldir}/org/kde/runnermodel/
#%%{_kf6_datadir}/kservicetypes6/plasma-runner.desktop

%files devel
%{_kf6_includedir}/KRunner/
#%%{_kf6_includedir}/KNewStuff/
%{_kf6_libdir}/libKF6Runner.so
%{_kf6_libdir}/cmake/KF6Runner/
%{_kf6_archdatadir}/mkspecs/modules/qt_KRunner.pri
%{_kf6_datadir}/dbus-1/interfaces/*
%{_kf6_datadir}/kdevappwizard/templates/runner6.tar.bz2
%{_kf6_datadir}/kdevappwizard/templates/runner6python.tar.bz2

%changelog
