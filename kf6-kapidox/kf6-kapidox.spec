%global gitdate 20230801.154407
%global cmakever 5.100.0
%global commit0 b6708c9551cf2af82cc68126403f7a3fa808c91e
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%global framework kapidox

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 34%{?dist}
Summary: KDE Frameworks 6 Tier 4 scripts and data for building API documentation
License: BSD-3-Clause
URL:     https://invent.kde.org/frameworks/%{framework}
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildArch:      noarch

BuildRequires:  kf6-rpm-macros
BuildRequires:  python3
BuildRequires:  python3-charset-normalizer
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

Requires:       kf6-filesystem
Requires:       python3-PyYAML
Requires:       python3-jinja2
Requires:       python3-requests

%description
Scripts and data for building API documentation (dox) in a standard format and
style.

%prep
%autosetup -n %{framework}-%{commit0} -p1

%build
%py3_build

%install
%py3_install

%files
%license LICENSES/*.txt
%{_kf6_bindir}/depdiagram_generate_all
%{_kf6_bindir}/kapidox-depdiagram-generate
%{_kf6_bindir}/kapidox-depdiagram-prepare
%{_kf6_bindir}/kapidox-generate
%{python3_sitelib}/kapidox
%{python3_sitelib}/kapidox*-py*.egg-info

%changelog
