%global gitdate 20230829.233310
%global cmakever 5.240.0
%global commit0 ce9850ab1a1425d84bb5cfdc1a07eda7bd9b3813
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%undefine __cmake_in_source_build
%global framework kpeople

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 128%{?dist}
Summary: KDE Frameworks 6 Tier 3 library for contact and people aggregation

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

# filter qml provides
%global __provides_exclude_from ^%{_kf6_qmldir}/.*\\.so$

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6ItemViews)
BuildRequires:  cmake(KF6Service)
BuildRequires:  cmake(KF6WidgetsAddons)
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtdeclarative-devel

%description
KDE Frameworks 6 Tier 3 library for interaction with XML RPC services.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%{cmake_kf6} \
    -DENABLE_EXAMPLES:BOOL=OFF
%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name



%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6People.so.*
%{_kf6_libdir}/libKF6PeopleWidgets.so.*
%{_kf6_libdir}/libKF6PeopleBackend.so.*
%{_kf6_qmldir}/org/kde/people/

%files devel
%{_kf6_includedir}/KPeople/
%{_kf6_libdir}/libKF6People.so
%{_kf6_libdir}/libKF6PeopleWidgets.so
%{_kf6_libdir}/libKF6PeopleBackend.so
%{_kf6_libdir}/cmake/KF6People/
%{_kf6_archdatadir}/mkspecs/modules/qt_KPeople.pri
%{_kf6_archdatadir}/mkspecs/modules/qt_KPeopleWidgets.pri


%changelog
