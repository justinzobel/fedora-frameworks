%global gitdate 20230911.100523
%global cmakever 5.240.0
%global commit0 386f450f719aa6e20bc85f51cdbecb98cde89457
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
# trim changelog included in binary rpms
%global _changelog_trimtime %(date +%s -d "1 year ago")

## allow building with an older extra-cmake-modules
%global kf6_version 5.33.0

%global framework breeze-icons

Name:    breeze-icon-theme
Summary: Breeze icon theme
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 138%{?dist}

# http://techbase.kde.org/Policies/Licensing_Policy
License: LGPLv3+
URL:     https://api.kde.org/frameworks-api/frameworks-apidocs/frameworks/breeze-icons/html/

%global versiondir %(echo %{version} | cut -d. -f1-2)
Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches (lookaside cache)

## upstreamable patches

BuildArch: noarch

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel

# icon optimizations
BuildRequires: hardlink
# for optimizegraphics
#BuildRequires: kde-dev-scripts
BuildRequires: time

# inheritance, though could consider Recommends: if needed -- rex
Requires: hicolor-icon-theme

# Needed for proper Fedora logo
Requires: system-logos

# upstream name
Provides:       breeze-icons = %{version}-%{release}
Provides:       kf6-breeze-icons = %{version}-%{release}

# upgrade path, since this no longer includes cursors since 5.16.0
Obsoletes:      breeze-icon-theme < 5.17.0

%description
%{summary}.


%prep
%autosetup -n %{framework}-%{commit0} -p1

%if 0%{?kf6_version:1}
sed -i -e "s|%{version}|%{kf6_version}|g" CMakeLists.txt
%endif


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

# Do not use Fedora logo from upstream
rm -rf %{buildroot}%{_datadir}/icons/breeze-dark/apps/48/org.fedoraproject.AnacondaInstaller.svg
rm -rf %{buildroot}%{_datadir}/icons/breeze/apps/48/org.fedoraproject.AnacondaInstaller.svg
# Use copy found in fedora-logos
pushd %{buildroot}%{_datadir}/icons/breeze-dark/apps/48/
ln -s ../../../hicolor/48x48/apps/org.fedoraproject.AnacondaInstaller.svg org.fedoraproject.AnacondaInstaller.svg
popd
pushd %{buildroot}%{_datadir}/icons/breeze/apps/48/
ln -s ../../../hicolor/48x48/apps/org.fedoraproject.AnacondaInstaller.svg org.fedoraproject.AnacondaInstaller.svg
popd

## icon optimizations
du -s .
hardlink -c -v %{buildroot}%{_datadir}/icons/
du -s .

# %%ghost icon.cache
touch  %{buildroot}%{_kf6_datadir}/icons/{breeze,breeze-dark}/icon-theme.cache


%check

%if 0%{?fedora} > 25 || 0%{?rhel} > 7
## trigger-based scriptlets
%transfiletriggerin -- %{_datadir}/icons/breeze
gtk-update-icon-cache --force %{_datadir}/icons/breeze &>/dev/null || :

%transfiletriggerin -- %{_datadir}/icons/breeze-dark
gtk-update-icon-cache --force %{_datadir}/icons/breeze-dark &>/dev/null || :

%transfiletriggerpostun -- %{_datadir}/icons/breeze
gtk-update-icon-cache --force %{_datadir}/icons/breeze &>/dev/null || :

%transfiletriggerpostun -- %{_datadir}/icons/breeze-dark
gtk-update-icon-cache --force %{_datadir}/icons/breeze-dark &>/dev/null || :

%else
## classic scriptlets
%post
touch --no-create %{_datadir}/icons/breeze &> /dev/null || :
touch --no-create %{_datadir}/icons/breeze-dark &> /dev/null || :

%posttrans
gtk-update-icon-cache %{_datadir}/icons/breeze &> /dev/null || :
gtk-update-icon-cache %{_datadir}/icons/breeze-dark &> /dev/null || :

%postun
if [ $1 -eq 0 ] ; then
  touch --no-create %{_datadir}/icons/breeze &> /dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/breeze &> /dev/null || :
  touch --no-create %{_datadir}/icons/breeze-dark &> /dev/null || :
  gtk-update-icon-cache %{_datadir}/icons/breze-dark &> /dev/null || :
fi
%endif

%files
%license COPYING-ICONS
%doc README.md
%ghost %{_datadir}/icons/breeze/icon-theme.cache
%ghost %{_datadir}/icons/breeze-dark/icon-theme.cache
%{_datadir}/icons/breeze/
%{_datadir}/icons/breeze-dark/
%{_libdir}/cmake/KF6BreezeIcons/KF6BreezeIconsConfig.cmake
%{_libdir}/cmake/KF6BreezeIcons/KF6BreezeIconsConfigVersion.cmake

%changelog
