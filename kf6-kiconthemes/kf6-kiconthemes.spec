%global gitdate 20230917.080341
%global cmakever 5.240.0
%global commit0 57123bc09e2eecb797df4b0607eeeb04896f021e
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kiconthemes

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary: KDE Frameworks 6 Tier 3 integration module with icon themes

License: LGPLv2+ and GPLv2+
URL:     https://api.kde.org/frameworks/kiconthemes/

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

## upstream patches

## upstreamable patches

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-karchive-devel
BuildRequires:  cmake(KF6ConfigWidgets)
BuildRequires:  cmake(KF6ConfigWidgets)
BuildRequires:  cmake(KF6CoreAddons)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6ItemViews)
BuildRequires:  cmake(KF6WidgetsAddons)
BuildRequires:  kf6-rpm-macros

BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qtbase-private-devel
BuildRequires:  qt6-qtsvg-devel

BuildRequires:  cmake(KF6ColorScheme)

BuildRequires:  cmake(Qt6Qml)
BuildRequires:  cmake(Qt6UiPlugin)

BuildRequires:  pkgconfig(xkbcommon)

Requires:       hicolor-icon-theme

%description
KDE Frameworks 6 Tier 3 integration module with icon themes

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6

%cmake_build


%install
%cmake_install

%find_lang %{name} --all-name



%files -f %{name}.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_bindir}/kiconfinder6
%{_kf6_libdir}/libKF6IconThemes.so.*
%{_kf6_libdir}/libKF6IconWidgets.so.*
%{_kf6_qtplugindir}/iconengines/KIconEnginePlugin.so
%{_kf6_qtplugindir}/designer/*6widgets.so
%{_kf6_libdir}/qt6/qml/org/kde/iconthemes

%files devel
%{_kf6_includedir}/KIconThemes
%{_kf6_includedir}/KIconWidgets
%{_kf6_libdir}/libKF6IconThemes.so
%{_kf6_libdir}/libKF6IconWidgets.so
%{_kf6_libdir}/cmake/KF6IconThemes/
%{_kf6_archdatadir}/mkspecs/modules/qt_KIconThemes.pri
%{_kf6_archdatadir}/mkspecs/modules/qt_KIconWidgets.pri

%changelog
