%global gitdate 20230829.233116
%global cmakever 5.240.0
%global commit0 5bf73aa7954870d2a1bd2208f41d6ad5bd8522f2
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kidletime

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 129%{?dist}
Summary: KDE Frameworks 6 Tier 1 integration module for idle time detection

License: GPLv2+ and LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)
Source0:  https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

# filter plugin provides
%global __provides_exclude_from ^(%{_kf6_plugindir}/.*\\.so)$

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  qt6-qtbase-devel
#BuildRequires:  qt6-qtx11extras-devel

BuildRequires:  cmake(Qt6WaylandClient)

BuildRequires:  pkgconfig(xext)
BuildRequires:  pkgconfig(x11-xcb)
BuildRequires:  pkgconfig(xcb)
BuildRequires:  pkgconfig(xcb-sync)
BuildRequires:  pkgconfig(Qt6WaylandClient)
BuildRequires:  pkgconfig(xscrnsaver)
BuildRequires:  pkgconfig(xkbcommon)

#BuildRequires:  libwayland-cursor
#BuildRequires:  libwayland-client
#BuildRequires:  libwayland-server
#BuildRequires:  libwayland-egl

BuildRequires:  wayland-devel
BuildRequires:  cmake(PlasmaWaylandProtocols)
BuildRequires:  wayland-protocols-devel
BuildRequires:  qt6-qtbase-private-devel

Requires:       kf6-filesystem

%description
KDE Frameworks 6 Tier 1 integration module for idle time detection.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       qt6-qtbase-devel
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%autosetup -n %{framework}-%{commit0} -p1


%build
%cmake_kf6

%cmake_build


%install
%cmake_install



%files
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6IdleTime.so.*
%dir %{_kf6_plugindir}/org.kde.kidletime.platforms/
%{_kf6_plugindir}/org.kde.kidletime.platforms/KF6IdleTimeXcbPlugin0.so
%{_kf6_plugindir}/org.kde.kidletime.platforms/KF6IdleTimeXcbPlugin1.so
%{_kf6_plugindir}/org.kde.kidletime.platforms/KF6IdleTimeWaylandPlugin.so


%files devel
%{_kf6_includedir}/KIdleTime/
%{_kf6_libdir}/libKF6IdleTime.so
%{_kf6_libdir}/cmake/KF6IdleTime/
%{_kf6_archdatadir}/mkspecs/modules/qt_KIdleTime.pri



%changelog
