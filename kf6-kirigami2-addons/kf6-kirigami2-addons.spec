%global gitdate 20230915.155315
%global cmakever 5.240.0
%global commit0 453e354b88d0fd17da12c72802c199fbd29d5d66
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%global orig_name kirigami-addons
%global framework kirigami-addons

Name:           kf6-kirigami2-addons
Version:        %{cmakever}^%{gitdate}.%{shortcommit0}
Release:        129%{?dist}
Epoch:          1
License:        LGPLv3
Summary:        Convergent visual components ("widgets") for Kirigami-based applications
Url:            https://invent.kde.org/libraries/kirigami-addons
Source:         kirigami-addons-%{shortcommit0}.tar.gz

BuildRequires: cmake
BuildRequires: extra-cmake-modules >= %{cmakever}
BuildRequires: gcc-c++
BuildRequires: kf6-rpm-macros
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6Kirigami2)
BuildRequires:  cmake(KF6I18n)
BuildRequires:  cmake(KF6Kirigami2)

BuildRequires:  cmake(Qt6Core)
BuildRequires:  cmake(Qt6Quick)
BuildRequires:  cmake(Qt6QuickControls2)

BuildRequires:  pkgconfig(xkbcommon)

%description
A set of "widgets" i.e visual end user components along with a
code to support them. Components are usable by both touch and
desktop experiences providing a native experience on both, and
look native with any QQC2 style (qqc2-desktop-theme, Material
or Plasma).

%package dateandtime
Summary:        Date and time add-on for the Kirigami framework
Requires:       %{name} = %{epoch}:%{version}-%{release}

%description dateandtime
Date and time Kirigami addons, which complements other
software like Kclock.

%package treeview
Summary:         Tree view add-on for the Kirigami framework
Requires:        %{name} = %{epoch}:%{version}-%{release}
%description treeview
Tree view Kirigami addon, which is useful for listing files.

%prep
%autosetup -n %{orig_name}-%{shortcommit0}

%build
%cmake_kf6 -DBUILD_WITH_QT6=ON  -DQT_MAJOR_VERSION=6
%cmake_build

%install
%cmake_install
%find_lang %{orig_name} --all-name

%files -f %{orig_name}.lang
%doc README.md
%license LICENSES/
%dir %{_kf6_qmldir}/org/kde
%dir %{_kf6_qmldir}/org/kde/kirigamiaddons
%{_kf6_libdir}/qt6/qml/org/kde/kirigamiaddons/*
%{_kf6_libdir}/cmake/KF6KirigamiAddons/*


%files dateandtime
%{_kf6_qmldir}/org/kde/kirigamiaddons/dateandtime/

%files treeview
%{_kf6_qmldir}/org/kde/kirigamiaddons/treeview/

%changelog
