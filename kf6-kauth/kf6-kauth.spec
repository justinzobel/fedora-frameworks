%global gitdate 20230830.100358
%global cmakever 5.240.0
%global commit0 3a2ee910211cb70164b87ab6d5b9934d36780c73
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global framework kauth

Name:    kf6-%{framework}
Version: %{cmakever}^%{gitdate}.%{shortcommit0}
Release: 133%{?dist}
Summary: KDE Frameworks 6 Tier 2 integration module to perform actions as privileged user

License: LGPLv2+
URL:     https://invent.kde.org/frameworks/%{framework}

%global majmin %(echo %{version} | cut -d. -f1-2)

Source0: https://invent.kde.org/frameworks/%{framework}/-/archive/%{commit0}/%{framework}-%{shortcommit0}.tar.gz

BuildRequires:  extra-cmake-modules >= %{cmakever}
BuildRequires:  gcc-c++
BuildRequires:  kf6-rpm-macros
BuildRequires:  polkit-qt6-1-devel
BuildRequires:  qt6-qtbase-devel
BuildRequires:  qt6-qttools-devel

BuildRequires:  cmake(KF6CoreAddons)

%description
KAuth is a framework to let applications perform actions as a privileged user.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
Requires:       cmake(KF6CoreAddons)
%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup -n %{framework}-%{commit0} -p1

%build
%cmake_kf6 \
  -DKDE_INSTALL_LIBEXECDIR=%{_kf6_libexecdir}
%cmake_build

%install
%cmake_install
%find_lang_kf6 kauth6_qt

%files -f kauth6_qt.lang
%doc README.md
%license LICENSES/*.txt
%{_kf6_datadir}/dbus-1/system.d/org.kde.kf6auth.conf
%{_kf6_datadir}/kf6/kauth/
%{_kf6_datadir}/qlogging-categories6/%{framework}.*
%{_kf6_libdir}/libKF6AuthCore.so.5*
%{_kf6_libdir}/libKF6AuthCore.so.6
%{_kf6_libdir}/qt6/plugins/kf6/kauth/backend/kauth_backend_plugin.so
%{_kf6_libexecdir}/kf6/kauth/kauth-policy-gen
%{_kf6_qtplugindir}/kf6/kauth/helper/kauth_helper_plugin.so

%files devel
%{_kf6_archdatadir}/mkspecs/modules/qt_KAuth*.pri
%{_kf6_includedir}/KAuth/
%{_kf6_includedir}/KAuthCore/
#%{_kf6_includedir}/KAuthWidgets/
%{_kf6_libdir}/cmake/KF6Auth/
%{_kf6_libdir}/libKF6AuthCore.so
#%{_kf6_libdir}/libKF6AuthWidgets.so


%changelog
